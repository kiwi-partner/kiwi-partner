$(function () {

    $(".mainMenulistitem").mouseenter(function () {
        $(this).children(".subMenulist").slideDown("fast");
    }).mouseleave(function () {
        $(this).children(".subMenulist").slideUp("fast");
    });
    
    var noti = 0;

    $(".noti").click(function () {
        $(this).toggleClass("is-active");

        if (noti == 0) {
            $(".modal.notification").slideDown("fast");
            noti = 1;
        } else if (noti == 1) {
            $(".modal.notification").slideUp("fast");
            noti = 0;
        }
    }).focusout(function() {
        $(".modal.notification").slideUp("fast");
        $(this).removeClass("is-active");
        noti = 0;
    });

    $("#menu").click(function () {
        $(".modal.menu").fadeIn("fast").css("display", "flex");
    });

    $("#blank").click(function () {
        $(".modal.menu").fadeOut("fast");
    });
});