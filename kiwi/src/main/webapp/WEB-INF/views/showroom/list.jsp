<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %> 
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<script>





</script>    

      <div class="mainWrap">
        <main class="showroomContents">
            <div class="map-container">
                <div class="title-container">
                    <p class="title">쇼룸 오시는 길</p>
                    <button class="mainbtn green" onclick="window.open('https://map.kakao.com/link/to/경기도 성남시 분당구 구미동 159,37.34003779279878,127.10696986310857')">길찾기</button>
                </div>
                <!-- 
               <img src="/resources/src/assets/showroom-map.jpg" alt="지도"/> 
                -->
				<div id="map" style="  width: 100%; height: 241px;border-radius: 6px;object-fit: cover;margin-bottom: 30px;"></div>
				  <script>
						var container = document.getElementById('map');
						var options = {
							center: new kakao.maps.LatLng(37.34003779279878,127.10696986310857),
							level: 3
						};
						var map = new kakao.maps.Map(container, options);
						// 마커가 표시될 위치입니다 
						var markerPosition  = new kakao.maps.LatLng(37.34003779279878,127.10696986310857); 
						// 마커를 생성합니다
						var marker = new kakao.maps.Marker({
						    position: markerPosition
						});
						// 마커가 지도 위에 표시되도록 설정합니다
						marker.setMap(map);

				 </script>  
            </div>
            <div class="info-container">
                <div class="title-container">
                    <p class="title">방문안내</p>
                </div>
                <ul>
                    <li>
                        <img src="/resources/src/assets/ic-map.svg" alt="오시는 길"/>
                        <p class="desc big">경기도 성남시 분당구 구미동 159 (CGV스퀘어) B동 2F 키위홈</p>
                    </li>
                    <li>
                        <img src="/resources/src/assets/ic-call.svg" alt="전화"/>
                        <p class="desc big">1822-8601</p>
                    </li>
                    <li>
                        <img src="/resources/src/assets/ic-parking.svg" alt="주차안내"/>
                        <p class="desc big">건물 기본 1시간 무료+쇼룸 방문 x시간 무료<br>
                            (y시간 이후) 추가 요금 10분 당 500원 부과</p>
                    </li>
                    <li>
                        <img src="/resources/src/assets/ic-clock.svg" alt="영업시간"/>
                        <p class="desc big"><b>평일:</b> 오전 11시 - 오후 8시<br>
                            <b>주말:</b> 오전 10시 - 오후 6시</p>
                    </li>
                </ul>
            </div>
        </main>
    </div>