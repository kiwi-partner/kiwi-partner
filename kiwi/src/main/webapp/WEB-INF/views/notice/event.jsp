<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %> 
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<script>


$(function(){
	 $(document).on("click", ".detail-read", function(){
        var seq= $(this).attr("data-seq");
        location.href="/notice/list/"+seq;
    });
})

$(function () {
    let container = $('.pagebtn-list');
    container.pagination({
        dataSource: function(done) {
        	var param = {
                    "type_seq" : "E"
            }
            ajaxCallPost("/api/v1/notice/list", param, function(res){
            	done(res.data);
            },function(){})
        },

        className: 'paginationjs-theme-green',

        callback: function (data, pagination) {
            var dataHtml = '<li class="event-listitem">';

            $.each(data, function (index, item) {
                var res = JSON.stringify(item.date_diff);
                console.log("Resresrs"+res);
            	if(parseInt(item.date_diff) < 0){
            		item.date_diff == "종료";
            	}else{
            		item.date_diff == '진행중';
            	}
                dataHtml += 
                	'<a href="#"class="detail-read" data-seq="'+ item.board_seq +'">'
                	+'<img src="'+item.img_url+'"alt="Thumb" />'
                    +'<div class="title-container"><p class="desc green">진행중</p>'
                    +'<p class="title small">'+item.title+'</p>'
                    +'<br>'
                    +'<p class="desc">'+item.start_dt_yyyymmdd+' ~ '+item.end_dt_yyyymmdd+'</p>'
                	+ '</div></a>';
            });

            dataHtml += '</li>';

            $(".event-list").html(dataHtml);
        }
    })
})
</script>    
 
    <div class="mainWrap">
        <main class="boardContents" id="event">
            <div class="board-container">
                <div class="menu-container">
                    <div class="title-container">
                        <p class="title">공지사항∙이벤트</p>
                    </div>
                    <ul>
                        <li><a href="/notice/list">공지사항</a></li>
                        <li class="is-active"><a href="/notice/event">이벤트</a></li>
                    </ul>
                </div>

                <div class="content-container">
                    <div class="title-container" id="event">
                        <p class="title"><!-- <button class="board-menu"><img src="/resources/src/assets/ic-mobile-menu.svg" alt="메뉴보기"/></button> -->이벤트</p>
                        <select class="mainbtn" style="background-color:#fff;">
                            <option value="doing">진행중인 이벤트만 보기</option>
                            <option value="done">진행완료 이벤트만 보기</option>
                            <option value="all">전체 보기</option>
                        </select>
                    </div>

                    <ul class="event-list" style="margin-bottom:50px;">
                    <!-- 리스트 -->
                   
                    </ul>
                     <ul class="pagebtn-list">
                    	<!-- 페이징 처리 영역  -->
                    </ul>
                </div>
            </div>
        </main>
    </div>