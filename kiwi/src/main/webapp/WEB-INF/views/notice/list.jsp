<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %> 
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<script>

$(function(){
	 $(document).on("click", ".read-info", function(){
        var seq= $(this).attr("data-seq");
        location.href="/notice/list/"+seq;
    });

	 

})
function init(){
    var param = {
            "type_seq" : "SN",
            "page" : 1,
            "page_block" : 10
    }
    ajaxCallPost("/api/v1/notice/list", param, function(res){
        $(".notice-list").html($("#notice-data").render(res));
        $(".pagebtn-list").html($("#page").render(res.paging));
    },function(){})
	
}
//init();
$(function () {
    let container = $('.pagebtn-list');
    container.pagination({
        dataSource: function(done) {
        	var param = {
                    "type_seq" : "SN"
            }
            ajaxCallPost("/api/v1/notice/list", param, function(res){
            	done(res.data);
            },function(){})
        },

        className: 'paginationjs-theme-green',

        callback: function (data, pagination) {
            var dataHtml = '<li class="notice-listitem">';

            $.each(data, function (index, item) {
                dataHtml += 
                	 '<a href="#"class="read-info" data-seq='+ item.board_seq +'>'
                	+'<div class="title-container"><p class="title small">'+item.title+'</p>'
                    +'<p class="desc">'+item.create_dt_yyyymmdd+'</p>'
                	+ '</div></a>';
            });

            dataHtml += '</li>';

            $(".notice-list").html(dataHtml);
        }
    })
})
</script>

    <div class="mainWrap">
        <main class="boardContents">
            <div class="board-container">
                <div class="menu-container">
                    <div class="title-container">
                        <p class="title">공지사항∙이벤트</p>
                    </div>
                    <ul>
                        <li class="is-active"><a href="/notice/list">공지사항</a></li>
                        <li><a href="/notice/event">이벤트</a></li>
                    </ul>
                </div>

                <div class="content-container">
                    <div class="title-container">
                        <p class="title"><!-- <button class="board-menu"><img src="/resources/src/assets/ic-mobile-menu.svg" alt="메뉴보기"/></button> -->공지사항</p>
                    </div>
					<ul class="notice-list" style="margin-bottom:30px;">
                    	<!-- 페이징 리스트 영역  -->
					</ul>
                    <ul class="pagebtn-list">
                    	<!-- 페이징 처리 영역  -->
                    </ul>
                  
                </div>
            </div>
        </main>
    </div>