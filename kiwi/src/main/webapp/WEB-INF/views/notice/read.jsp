<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %> 
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<script>

$(function(){
	 $(document).on("click", ".hold-back-btn", function(){
       location.href="/notice/list";
  	 });
})



</script>    
      <div class="mainWrap">
        <main class="boardContents">
            <div class="board-container">
                <div class="menu-container">
                    <div class="title-container">
                        <p class="title">공지사항∙이벤트</p>
                    </div>
                    <ul>
                        <li><a href="/notice/list">공지사항</a></li>
                        <li><a href="/notice/event">이벤트</a></li>
                    </ul>
                </div>

                <div class="content-container read">
                    <div class="title-container">
                        <span>
                            <button class="mainbtn transprent" onclick="history.back(-1); return false"><img src="/resources/src/assets/ic-back.svg" /></button>
                            <span class="title">${detail.title }</span>
                        </span>
                        <p class="desc">${detail.create_dt_yyyymmdd }</p>
                    </div>
                    <div class="paragraph-container">
                        <p class="desc big">
                        	${detail.content }	
                          </p>
                    </div>
                </div>
            </div>
        </main>
    </div>