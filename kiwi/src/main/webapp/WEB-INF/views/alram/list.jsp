<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %> 
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<script>
/* 알림 확인하고 페이지 이동 
$(function(){
	 $(document).on("click", ".noti_info", function(){
        var seq = $(this).attr("data-seq");
        var type = $(this).attr("data-type");
        ajaxCallGet("/api/v1/alarm/check?seq="+seq, function(res){
			if(res.success){
				if(type == 'M'){
				  location.href="/mykiwi";
				}
				if(type == 'K'){
				 location.href="/";
				}
				
			}
		})
    });
})*/
</script>    
    
 			<c:forEach var="item" items="${alramList }">
                        <li class="notificationListitem">
                            <div class="container noti_info" data-seq="${item.noti_seq}" data-type="${item.noti_type}">
                                <span class="content">
                                    <span class="work-id">${item.title }</span>
                                    <span class="work-info">
                                        <div class="title tiny">${item.content }</div>
                                        <div class="desc small">${item.create_dt_yyyymmdd  }
                                        	<c:if test="${item.show_yn eq Y}">NEW</c:if></div>
                                    </span>
                                </span>
                                <a>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="6.792" height="11"
                                        viewBox="0 0 6.792 11">
                                        <path id="Icon_material-keyboard-arrow-right"
                                            data-name="Icon material-keyboard-arrow-right"
                                            d="M12.885,18.333l4.2-4.208-4.2-4.207,1.293-1.293,5.5,5.5-5.5,5.5Z"
                                            transform="translate(-12.885 -8.625)" fill="#606060" />
                                    </svg>

                                </a>
                            </div>
                        </li>
			</c:forEach>