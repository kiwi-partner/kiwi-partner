<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %> 
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<script>
/*카카오 지도 */
function goPopup(){
    new daum.Postcode({
        oncomplete: function(data) {
			document.getElementById("address").value = data.address;
        }
    }).open();
}
/* nice 인증 */
function fnPopup(){
	var pop = window.open("/nice","pop","width=570,height=420, scrollbars=yes, resizable=yes"); 
}
	
/* nice 인증 return */
function signDataCallBack(phone,name){
	document.getElementById("phone").value = phone;	
	document.getElementById("name").value = name;	
	$(".send-phone").text("변경 완료");
	isPhoneAuth = true;
}

$(function() {
	$(document).on("click", ".category", function(){
        var category_seq = $("#work option:selected").val();
      
        var param = {
        		"category_seq" : category_seq
        }
		ajaxCallPost("/api/v1/category/info", param, function(res){
			if(res.success){
				$(".info-list").html($("#info_data").render(res));
			}
        },function(){}) 
	})
   
    
	$(document).on("click", ".regi_btn", function(){
		var member_seq = "${sessionScope.MEMBER.member_seq}";
		var name = $("input[name=name]");
		var phone = $("input[name=phone]");
		var addr = $("input[name=addr]");
		var addr_info = $("input[name=addr_info]");
		var store_name = $("input[name=store_name]");
		var tel = $("input[name=tel]");
        var category_seq = $("#work").val();
        var info_seq = $("#detailwork").val();
		var business_number = $("input[name=business_number]");
		var job_seq = $("input[name=job_seq]");
		if(!store_name.val()){
			alert('업체명을 입력해주세요.');
			store_name.focus();
			return;
		}
		if(!tel.val()){
			alert('업체 번호를 입력해주세요.');
			tel.focus();
			return;
		}
		if(!work){
			alert('업무를 입력해주세요.');
			work.focus();
			return;
		}

		if(!detailwork){
			alert('업무를 입력해주세요.');
			detailwork.focus();
			return;
		}
		if(!business_number.val()){
			alert('사업자 번호를 입력해주세요.');
			business_number.focus();
			return;
		}
		if(!addr.val()){
			alert('주소를 입력 해주세요.');
			addr.focus();
			return;
		}
		
        var param = {
        		"member_seq" : member_seq,
                "name" : name.val(),
                "phone" : phone.val(),
                "addr" : addr.val(),
                "addr_info" : addr_info.val(),
                "store_name" : store_name.val(),
                "tel" : tel.val(),
                "category_seq" : category_seq,
                "info_seq" :info_seq,
                "job_seq" :job_seq.val(),
                "business_number" : business_number.val()
        }

         ajaxCallPost("/api/v1/member/update", param, function(res){
            if(res.success){
            	alert("회원 정보 변경되었습니다.");
        		//location.href="/";
            }
        },function(){}) 
    })

    $(document).on("click", ".withdraw", function(){
    	if(confirm("키위 파트너 회원 탈퇴하시겠습니까?")){
   
        var seq = "${sessionScope.MEMBER.member_seq}";
        var param = {
                "seq" : seq
        }
        ajaxCallPost("/api/v1/member/delete", param, function(res){
       		alert("회원 탈퇴 되었습니다.")
       		location.href="/";
        },function(){})
     	}else{
    	 return;
     	}
    	});
   
});




</script>

  <div class="mainWrap">
        <main class="regiContents">
            <div class="title-container">
                <p class="title">정보 변경</p>
            </div>

            <ul class="regi-list">

                <li class="regi-listitem">
                    <p class="title small">이름</p>
                    <input class="input" type="text" id="name" name="name" value="${detail.name }" disabled="disabled"/>
                    <input type="hidden" name="job_seq" value="${detail.job_seq }">
                </li>

                <li class="regi-listitem">
                    <p class="title small">휴대폰번호</p>
                    <div>
                        <input class="input" type="text" id="phone" name="phone" value="${detail.phone }" disabled="disabled"/>
                        <button class="mainbtn greeninvert send-phone" onclick="fnPopup()">휴대폰 번호변경</button>
                    </div>
                </li>
				<!-- 
                <li class="regi-listitem">
                    <p class="title small">인증번호</p>
                    <div>
                        <div class="input"> <input class="input transparent" type="number" id="verification" placeholder="인증번호"/><span class="desc" id="smstimeout" style="color: red;">3:00</span></div>
                        <button class="mainbtn greeninvert">인증번호 다시받기</button>
                    </div>
                </li>
				 -->
                <li class="regi-listitem">
                    <p class="title small">주소</p>
                    <div>
                        <div class="input-container">
                            <div class="input"><input class="input transparent" name="addr" type="text" id="address"  value="${detail.addr }"/>
                             <button class="iconbtn" onclick="goPopup()"><img src="/resources/src/assets/ic-search.svg"/></button></div>
                            <input class="input" type="text" id="extra-address" name="addr_info" placeholder="상세주소" value="${detail.addr_info }" />
                        </div>
                    </div>
                </li>

                <li class="regi-listitem">
                    <p class="title small">업체명</p>
                    <!-- 
                    <input class="input" type="hidden" name="store_seq" value="${detail.store_seq }"/> -->
                    <input class="input" type="text" id="companyname" name="store_name" value="${detail.store_name }"/>
                </li>

                <li class="regi-listitem">
                    <p class="title small">업체 전화번호</p>
                    <input class="input" type="text" id="companytel" name="tel" value="${detail.tel }" />
                </li>

                <li class="regi-listitem">
                    <p class="title small">사업자 번호</p>
                    <input class="input" type="text" id="companytel" name="business_number" value="${detail.business_number }" />
                </li>

                <li class="regi-listitem">
                    <p class="title small">사업자 등록증</p>
                    <input type="hidden" value="${detail.file_url }">
                    <button class="mainbtn greeninvert" style="width: 100%;">보기</button>
                </li>

                <li class="regi-listitem">
                    <p class="title small">주업무</p>
                    <select class="input category" id="work" required style="background-color: #fff;">
                		<option disabled>${detail.category_name}</option>
                        <c:forEach var="item" items="${categoryList }">
                     	   <option value="${item.category_seq }">${item.name }</option>
                    	</c:forEach>
                    </select>
                </li>

                <li class="regi-listitem">
                    <p class="title small">세부업무</p>
                    <select class="input info-list" id="detailwork" style="background-color: #fff;">
                		<option>${detail.info_name}</option>
                        
                    </select>
                    <script id="info_data" type="text/x-jsrender">
						{{for data}}
						<option value="{{:info_seq}}">{{:name}}</option>
						{{/for}}
					</script>
                </li>
            </ul>

            <a href="#"><button class="mainbtn green regi_btn" id="registration">회원 정보 수정 </button></a>

            <div class="extra-container" style="margin-bottom: 100px;">
                <a href="#" class="withdraw" id="withdraw">회원 탈퇴</a>
            </div>
        </main>
    </div>