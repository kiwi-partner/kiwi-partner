<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %> 
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<jsp:useBean id="today" class="java.util.Date" />
<fmt:formatDate value="${today}" pattern="yyyyMMddHHmmss" var="nowDate"/>

<script>
var isPhoneAuth = false;
var isIDcheck = false;
var authRandNumber = "${nowDate}";
/*카카오 지도 */
function goPopup(){
//	var pop = window.open("/resources/juso.html","pop","width=570,height=420, scrollbars=yes, resizable=yes"); 
    new daum.Postcode({
        oncomplete: function(data) {
            // 팝업에서 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분입니다.
            // 예제를 참고하여 다양한 활용법을 확인해 보세요.
			document.getElementById("address").value = data.address;
        }
    }).open();
}
/* function jusoCallBack(roadFullAddr){
	$("input[name=address]").value = roadFullAddr;	
} */

/* nice 인증 */
function fnPopup(){
	var pop = window.open("/nice","pop","width=570,height=420, scrollbars=yes, resizable=yes"); 
}
	
/* nice 인증 return */
function signDataCallBack(phone,name){
	document.getElementById("phone").value = phone;	
	document.getElementById("name").value = name;	
	$(".send-phone").text("인증 완료");
	isPhoneAuth = true;
}


$(function(){
	$(document).on("click", ".category", function(){
        var category_seq = $("#work").val();
      
        var param = {
        		"category_seq" : category_seq
        }
		ajaxCallPost("/api/v1/category/info", param, function(res){
				$("#detailwork").html($("#info_data").render(res.data));
        },function(){}) 
	})
	$(document).on("click", ".common_chk", function(){
		var id = $("input[name=id]");
      
	 	var param = {
		  "column_name":"id",
		  "column_value":id.val()
		} 
		ajaxCallPost("/api/v1/common/chk", param, function(res){
			if(res.success){
				alert("중복된 아이디가 존재합니다.");
			}else{
				alert("사용가능한 아이디 입니다.");
				$("input[name=id]").val(id.val());
				isIDcheck = true;
			}

        },function(){}) 
	})
	/* $(document).on("keyup", ".send-phone", function(){
		isPhoneAuth = false;
	})
	$(document).on("keyup", ".common_chk", function(){
		alert('ID 중복확인 해주세요 ');
		id.focus();
	}) */
	$(document).on("click", ".regi_btn", function(){
		var id = $("input[name=id]");
		var pw = $("input[name=pw]");
		var repw = $("input[name=repw]");
		var name = $("input[name=name]");
		var phone = $("input[name=phone]");
		var addr = $("input[name=addr]");
		var addr_info = $("input[name=addr_info]");
		var store_name = $("input[name=store_name]");
		var tel = $("input[name=tel]");
        var category_seq = $("#work").val();
        var info_seq = $("#detailwork").val();
		var business_number = $("input[name=business_number]");
		var file_url = $("input[name=file_url]");
		
		if(!id.val()){
			alert('ID을 입력해주세요');
			id.focus();
			return;
		}
		
		if(!pw.val()){
			alert('비밀번호를 입력해주세요');
			pw.focus();
			return;
		}
		if(!repw.val()){
			alert('비밀번호를 확인 입력 해주세요');
			repw.focus();
			return;
		}
		if(!store_name.val()){
			alert('업체명을 입력해주세요.');
			store_name.focus();
			return;
		}
		if(!tel.val()){
			alert('업체 번호를 입력해주세요.');
			tel.focus();
			return;
		}
		if(!work){
			alert('업무를 입력해주세요.');
			work.focus();
			return;
		}

		if(!detailwork){
			alert('업무를 입력해주세요.');
			detailwork.focus();
			return;
		}
		if(!business_number.val()){
			alert('사업자 번호를 입력해주세요.');
			business_number.focus();
			return;
		}
		if(!file_url.val()){
			alert('사업자등록증을 확인 해주세요.');
			file_url.focus();
			return;
		}
		if(!addr.val()){
			alert('주소를 입력 해주세요.');
			addr.focus();
			return;
		}
		if(pw.val() != repw.val()){
			console.log(">>>>>>>>>>>>>"+pw+">>>>>>>>>"+repw);
			alert('비밀번호가 일치 하지 않습니다.');
			repw.focus();
			return;
		}
		if(!$("#agreement").is(":checked")){
			alert("키위홈 파트너 개인정보수집 및 이용동의에\n동의하여 주세요.")
			return;
		}
		if(!$("#private").is(":checked")){
			alert("키위홈 파트너 서비스 이용약관에\n동의하여 주세요.")
			return;
		}
		if(!$("#attribute").is(":checked")){
			alert("키위홈 파트홈 계약동의사항에 \n동의하여 주세요.")
			return;
		}
		if(!isPhoneAuth){
			alert('전화번호 인증을 다시 시도해주세요.');
			phone.focus();
			return;
		}
		if(!isIDcheck){
			alert('ID 중복체크 해주세요.');
			id.focus();
			return;
		}
        var param = {
                "id" : id.val(),
                "pw" : pw.val(),
                "name" : name.val(),
                "phone" : phone.val(),
                "addr" : addr.val(),
                "addr_info" : addr_info.val(),
                "store_name" : store_name.val(),
                "tel" : tel.val(),
                "category_seq" : category_seq,
                "info_seq" :info_seq,
                "business_number" : business_number.val(),
                "file_url" : file_url.val()
        }

         ajaxCallPost("api/v1/sign", param, function(res){
            if(res.success){
        		location.href="/";
            }
        },function(){}) 
    })
 	$(".business_btn").change(function(){
    		ajaxUpload($(".business_btn")[0].files[0], "business", function(data_url) {
    			$("input[name='file_url']").val(data_url);
    	})
    })

    $(".privacy_btn").click(function(){
    	window.open('/resources/privacy.html', '개인정보처리', 'width=300px,height=600px,scrollbars=yes');
    })
    $(".agree_btn").click(function(){
    	window.open('/resources/agreement.html', '서비스이용약관', 'width=300px,height=600px,scrollbars=yes');
    })
})
</script>

       				 


    <div class="mainWrap">
        <main class="regiContents">
            <img src="/resources/src/assets/logotype.svg" />

            <ul class="regi-list">
                <li class="regi-listitem">
                    <p class="title small">아이디 <span style="color: red;">*</span></p>
                    <div>
                        <div class="input"><input class="input transparent" type="text" id="id" name="id" placeholder="아이디" /></div>
                        <button class="mainbtn greeninvert common_chk">중복확인</button>
                    </div>
                </li>

                <li class="regi-listitem">
                    <p class="title small">비밀번호 <span style="color: red;">*</span></p>
                    <input class="input" type="password" name="pw" id="password" placeholder="비밀번호(8-16자리 영문, 숫자조합)" />
                </li>

                <li class="regi-listitem">
                    <p class="title small">비밀번호 확인 <span style="color: red;">*</span></p>
                    <input class="input" type="password" name="repw" id="re-password" placeholder="비밀번호 확인" />
                </li>

                <li class="regi-listitem">
                    <p class="title small">이름 <span style="color: red;">*</span></p>
                    <input class="input" type="text" name="name" id="name" disabled="disabled" placeholder="본인인증 해주세요."/>
                </li>

                <li class="regi-listitem">
                    <p class="title small">전화번호 <span style="color: red;">*</span></p>
                    <div>
                        <input class="input" type="tel" name="phone" id="phone" disabled="disabled" />
                        <button class="mainbtn greeninvert send-phone" onclick="fnPopup()">본인 인증</button>
                    </div>
                </li>
				<!-- 
                <li class="regi-listitem">
                    <p class="title small">인증번호 <span style="color: red;">*</span></p>
                    <div>
                        <div class="input"> <input class="input transparent" type="text" name="authNumber" id="verification" placeholder="인증번호"/><span class="desc" id="smstimeout" style="color: red;">3:00</span></div>
                        <button class="mainbtn greeninvert auth-number-chk">인증완료</button>
                    </div>
                </li>
 				-->
                <li class="regi-listitem">
                    <p class="title small">업체명 <span style="color: red;">*</span></p>
                    <input class="input" name="store_name" type="text" id="companyname" placeholder="업체명" />
                </li>

                <li class="regi-listitem">
                    <p class="title small">업체 전화번호 <span style="color: red;">*</span></p>
                    <input class="input" name="tel" type="tel" id="companytel" placeholder="업체 전화번호" />
                </li>

                <li class="regi-listitem">
                    <p class="title small">사업자 번호 <span style="color: red;">*</span></p>
                    <input class="input" name="business_number" type="tel" id="companytel" placeholder="ex: 123-45-678900" />
                </li>

                <li class="regi-listitem">
                    <p class="title small">주업무 <span style="color: red;">*</span></p>
                    <select class="input category" type="number" id="work" style="background-color: #fff;">
					<c:forEach var="item" items="${categoryList }">
                        <option value="${item.category_seq }">${item.name }</option>
                    </c:forEach>
                    </select>
                </li>

                <li class="regi-listitem">
                    <p class="title small">세부업무 <span style="color: red;">*</span></p>
                    <select class="input info" type="number" id="detailwork" style="background-color: #fff;">
                        <!-- 렌더링 진행 -->
                    </select>
                    <script id="info_data" type="text/x-jsrender">
						<option value="{{:info_seq}}">{{:name}}</option>
					</script>
                </li>

                <li class="regi-listitem">
                    <p class="title small">주소 <span style="color: red;">*</span></p>
                    <div>
                        <div class="input-container">
                            <div class="input"><input class="input transparent" type="text" name="addr" id="address"
                                    placeholder="주소" /> <button class="iconbtn" onclick="goPopup()"><img
                                        src="/resources/src/assets/ic-search.svg" /></button></div>
                            <input class="input" type="text" name="addr_info" id="extra-address" placeholder="상세주소" />
                        </div>
                    </div>
                </li>

               	 <li class="regi-listitem agreement">
		               <p class="desc big">서비스 이용약관 및 개인정보 수집 및 이용 동의를 읽고 동의해 주시기
		                  바랍니다.</p>
		               <div>
		                  <button class="mainbtn invert agree_btn"><span>서비스 이용약관 <span style="color: red;">(필수)</span></span><img
                                src="/resources/src/assets/ic-arrow-right.svg" /></button>
		                  <input type="checkbox" name="agreement" id="agreement" > <label
		                     for="agreement" class="mainbtn invert checkbox"><img
		                     src="/resources/src/assets/ic-check.svg" /> </label>
		               </div>
		               <div>
		                  <button class="mainbtn invert privacy_btn">
		                   <span>개인정보 수집 및 이용 동의 <span style="color: red;">(필수)</span></span><img
		                        src="/resources/src/assets/ic-arrow-right.svg" />
		                  </button>
		                  <input type="checkbox" class="mainbtn invert checkbox" 
		                     name="private" id="private"> <label for="private"
		                     class="mainbtn invert checkbox"><img
		                     src="/resources/src/assets/ic-check.svg" /> </label>
		               </div>
		                <div>
		                  <button class="mainbtn invert"><span>키위홈 계약 동의 <span style="color: red;">(필수)</span></span><img
                                src="/resources/src/assets/ic-arrow-right.svg" /></button>
		                  <input type="checkbox" class="mainbtn invert checkbox" 
		                     name="private" id="attribute"> <label for="attribute"
		                     class="mainbtn invert checkbox"><img
		                     src="/resources/src/assets/ic-check.svg" /> </label>
		               </div>
                    <div>
          				<input type="hidden" name="file_url">
                        <input type="file" style="display:none;" class="business_btn">
            			<button class="mainbtn invert"><span>사업자등록증</span></button>
                        <button class="mainbtn invert attach" id="bus_file" type="button">첨부</button>
                    </div>
                </li>
            </ul>
	         <a href="#"><button class="mainbtn green regi_btn" id="registration">회원가입신청</button></a>
        </main>
    </div>