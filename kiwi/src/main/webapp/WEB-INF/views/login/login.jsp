<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %> 
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<script>
$(function(){

    $(".login_btn").click(function(){
		var id = $("input[name=id]");
		var pw = $("input[name=pw]");
        var param = {
                "id" : id.val(),
                "pw" : pw.val(),
        }
        ajaxCallPost("/api/v1/login", param, function(res){
            if(res.success){
        		location.href="/";
            }else{
            	alert("아이디, 비밀번호를 확인해주세요 ");
            }
        },function(){})
    })


})




</script>

    <div class="mainWrap">
        <main class="loginContents">
            <div class="login-container">
                <img src="/resources/src/assets/login-banner.png">

                <div class="input-container">
                    <input class="input green" type="email" name="id" id="id" placeholder="아이디" />
                    <input class="input green" type="password" name="pw" id="password" placeholder="비밀번호" />
                </div>

                <div class="button-container">
                    <button class="mainbtn green login_btn">협력업체 로그인</button>
                    <a href="/sign"><button class="mainbtn greeninvert">회원가입</button></a>
                </div>

                <div class="extra-container">
                    <a href="#" class="desc">이용약관</a>
                    <a href="#" class="desc">개인정보보호정책</a>
                </div>
            </div>
        </main>
    </div>