
<!DOCTYPE html>
<%@ page language="java" contentType="text/html;charset=euc-kr" %>

<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>nice 인증 </title>

    <link rel="icon" href="/favicon.ico" />

    <script src="/resources/js/commonAjax.js"></script>
    <script src="/resources/js/custom.js"></script>
    
    <link rel="stylesheet" type="text/css" href="/resources/css/style.css" />
    
    <script>

    function init(){
    	document.form.action = "https://nice.checkplus.co.kr/CheckPlusSafeModel/checkplus.cb";
    	document.form.submit();
    }
   // init();
    
    </script>
</head>

<body onload="init();">
	<form id="form" name="form" method="post">
		<input type="hidden" name="m" value="checkplusService">					
		<input type="hidden" name="EncodeData" value="${data}">	
	</form>
</body>
</html>

 