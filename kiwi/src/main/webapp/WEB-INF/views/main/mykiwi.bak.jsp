<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %> 
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<script>


</script>

   
    <div class="mainWrap">
        <main class="dashboardContents">
            <div class="dashboard-container">
                <div class="brief-container">
                    <div class="title-container"><span class="title">작업 현황</span></div>
                    <div class="search-container">
                        <input class="input" type="text" id="search" placeholder="검색어를 입력하세요."/>
                        <div class="period-container">
                            <div class="schedule">
                                <input class="mainbtn" type="date" placeholder="시작일">
                                <span class="desc green">~</span>
                                <input class="mainbtn" type="date" placeholder="종료일">
                                <button id="calendar" class="mainbtn transprent"><img src="/resources/src/assets/ic-schedule.svg"/></button>
                            </div>
                        </div>
                    </div>
                    <div class="summary-container">
                        <table>
                            <tr>
                                <th>미정산 시공비</th>
                                <td data-name="unsettled-pay">000,000,000원</td>
                                <th>미정산 작업 수</th>
                                <td data-name="unsettled-time">0건</td>
                            </tr>
                            <tr>
                                <th>마지막 정산일</th>
                                <td data-name="last-settled">2020-01-24</td>
                                <th>정산신청</th>
                                <td data-name="request-settile"><button class="mainbtn green">신청</button></td>
                            </tr>
                            <tr>
                                <th>전체 시공비</th>
                                <td data-name="total-paid">000,000,000원</td>
                                <th>전체 작업 횟수</th>
                                <td data-name="totla-worked">0건</td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="works-container">
                    <ul>
                        <li>
                            <table>
                                <tr>
                                    <th colspan="4" class="title">작업 기록 0006</th>
                                </tr>
                                <tr>
                                    <th>작업 기간</th>
                                    <td>2020-01-05 ~<br>2020-01-06</td>
                                    <th>식별 번호</th>
                                    <td>I20200104000001</td>
                                </tr>
                                <tr>
                                    <th>주소</th>
                                    <td colspan="3">경기도 성남시 분당구 미금로</td>
                                </tr>
                                <tr>
                                    <th>작업내용</th>
                                    <td colspan="3">20m2, xxx(자재)</td>
                                </tr>
                                <tr>
                                    <th>시공비</th>
                                    <td>50,000원</td>
                                    <th>정산 여부</th>
                                    <td>Y</td>
                                </tr>
                                <tr>
                                    <th>고객 성함</th>
                                    <td colspan="3">홍길동</td>
                                </tr>
                                <tr>
                                    <th colspan="4" class="done">작업 완료 <img src="/resources/src/assets/ic-check-green.svg" alt="체크마크"/></ㅌ>
                                </tr>
                            </table>
                        </li>
                        <li>
                            <table>
                                <tr>
                                    <th colspan="4" class="title">작업 기록 0006</th>
                                </tr>
                                <tr>
                                    <th>작업 기간</th>
                                    <td>2020-01-05 ~<br>2020-01-06</td>
                                    <th>식별 번호</th>
                                    <td>I20200104000001</td>
                                </tr>
                                <tr>
                                    <th>주소</th>
                                    <td colspan="3">경기도 성남시 분당구 미금로</td>
                                </tr>
                                <tr>
                                    <th>작업내용</th>
                                    <td colspan="3">20m2, xxx(자재)</td>
                                </tr>
                                <tr>
                                    <th>시공비</th>
                                    <td>50,000원</td>
                                    <th>정산 여부</th>
                                    <td>Y</td>
                                </tr>
                                <tr>
                                    <th>고객 성함</th>
                                    <td colspan="3">홍길동</td>
                                </tr>
                                <tr>
                                    <th colspan="4" class="undone">작업 완료 확인</th>
                                </tr>
                            </table>
                        </li>
                        <li>
                            <table>
                                <tr>
                                    <th colspan="4" class="title">작업 기록 0006</th>
                                </tr>
                                <tr>
                                    <th>작업 기간</th>
                                    <td>2020-01-05 ~<br>2020-01-06</td>
                                    <th>식별 번호</th>
                                    <td>I20200104000001</td>
                                </tr>
                                <tr>
                                    <th>주소</th>
                                    <td colspan="3">경기도 성남시 분당구 미금로</td>
                                </tr>
                                <tr>
                                    <th>작업내용</th>
                                    <td colspan="3">20m2, xxx(자재)</td>
                                </tr>
                                <tr>
                                    <th>시공비</th>
                                    <td>50,000원</td>
                                    <th>정산 여부</th>
                                    <td>Y</td>
                                </tr>
                                <tr>
                                    <th>고객 성함</th>
                                    <td colspan="3">홍길동</td>
                                </tr>
                                <tr>
                                    <th colspan="4" class="undone">작업 완료 확인</th>
                                </tr>
                            </table>
                        </li>
                        <li>
                            <table>
                                <tr>
                                    <th colspan="4" class="title">작업 기록 0006</th>
                                </tr>
                                <tr>
                                    <th>작업 기간</th>
                                    <td>2020-01-05 ~<br>2020-01-06</td>
                                    <th>식별 번호</th>
                                    <td>I20200104000001</td>
                                </tr>
                                <tr>
                                    <th>주소</th>
                                    <td colspan="3">경기도 성남시 분당구 미금로</td>
                                </tr>
                                <tr>
                                    <th>작업내용</th>
                                    <td colspan="3">20m2, xxx(자재)</td>
                                </tr>
                                <tr>
                                    <th>시공비</th>
                                    <td>50,000원</td>
                                    <th>정산 여부</th>
                                    <td>Y</td>
                                </tr>
                                <tr>
                                    <th>고객 성함</th>
                                    <td colspan="3">홍길동</td>
                                </tr>
                                <tr>
                                    <th colspan="4" class="undone">작업 완료 확인</th>
                                </tr>
                            </table>
                        </li>
                    </ul>
                </div>
            </div>
        </main>
    </div>