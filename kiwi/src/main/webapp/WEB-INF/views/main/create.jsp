<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %> 
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>


   <div class="mainWrap">
        <main class="boardContents">
            <div class="board-container calendar">
                <div class="menu-container">
                    <div class="title-container calendar">
                        <button id="btnPrev" class="mainbtn transparent"><img
                                src="/resources/src/assets/ic-arrow-left-black.svg" alt="이전" /></button>
                        <span class="title" id="month">12월</span>
                        <button id="btnNext" class="mainbtn transparent"><img
                                src="/resources/src/assets/ic-arrow-right-black.svg" alt=다음 /></button>
                    </div>
                    <div class="calendar-container">
                        <div id="divCal"></div>
                    </div>
                </div>

                <div class="content-container">
                    <div class="title-container">
                        <p class="title">일정 추가</p>
                        <div class="button-container">
                            <button class="mainbtn">일정 등록 취소</button>
                            <button class="mainbtn green">일정 등록</button>
                        </div>
                    </div>

                    <div class="input-container">
                        <div class="title">제목</div>
                        <input class="input" placeholder="예) 사무실 이사" />
                    </div>
                    <div class="input-container">
                        <div class="title">날짜</div>
                        <div class="schedule">
                            <div class="input-container">
                                <input class="mainbtn" type="date" placeholder="시작일">
                                <span class="desc green">~</span>
                                <input class="mainbtn" type="date" placeholder="종료일">
                                <button id="calendar" class="mainbtn green">일정추가</button>
                            </div>
                            <ul>
                                <li>
                                    <div class="mainbtn" id="daystart">2020-04-27</div><span class="desc green">~</span><div
                                        class="mainbtn" id="dayend">2020-04-28</div>
                                        <button id="delete" class="mainbtn transparent"><img src="/resources/src/assets/ic-close.svg" alt="삭제"/></button>
                                </li>
                                <li>
                                    <div class="mainbtn" id="daystart">2020-04-27</div><span class="desc green">~</span><div
                                        class="mainbtn" id="dayend">2020-04-28</div>
                                        <button id="delete" class="mainbtn transparent"><img src="/resources/src/assets/ic-close.svg" alt="삭제"/></button>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="input-container">
                        <div class="title">메모</div>
                        <textarea class="input" placeholder="예) 사무실 이사"></textarea>
                    </div>
                </div>
            </div>
        </main>
    </div>