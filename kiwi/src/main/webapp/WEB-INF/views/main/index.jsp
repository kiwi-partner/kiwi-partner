<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<script>


$(function () {
    $("#deny").click(function () {
        $(".content-container > .modalWrap").fadeIn("fast").css("display", "flex");
    });
    $("#yes").click(function () {
        $(".modalWrap").fadeOut("fast");
        alert('작업이 취소되었습니다');
    });

    $("#cancel").click(function () {
        $(".modalWrap").fadeOut("fast");
    });
    
    $('.consulting_list').click(function(){
    	$('.consulting_list').removeClass("selected");
    	$('.normal').css({"color":"black"});
    	$('.today').css({"color":"#40B265"});
    	
    	var class_name = $(this).attr("class");
    	//console.log($(this).attr('data-id'));
    	var listDate = [];
    	var start_dt = $(this).attr("data-id").split('/')[0];
    	var end_dt = $(this).attr("data-id").split('/')[1];
    	getDateRange(start_dt,end_dt,listDate);
    	
    	if(class_name.indexOf("selected") > -1) {
    		for(var i=0;i<listDate.length;i++){
    			console.log(listDate[i])
    			$('td[data-id="' + listDate[i] + '"]').css({"color" : "black"});
    		}
    		$(this).removeClass("selected");
    	}
    	else {
    		for(var i=0;i<listDate.length;i++){
    			console.log(listDate[i])
    			$('td[data-id="' + listDate[i] + '"]').css({"color" : "red"});
    		}
    		$(this).addClass("selected");
    	}
    	
    });
    
    $('.day').click(function(){
    	console.log($(this).attr('data-id'));
    });


});


var Cal = function (divId) {

    //Store div id
    this.divId = divId;

    // Days of week, starting on Sunday
    this.DaysOfWeek = [
        '일',
        '월',
        '화',
        '수',
        '목',
        '금',
        '토'
    ];

    // Months, stating on January
    this.Months = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12'];

    // Set the current month, year
    var d = new Date();

    this.currMonth = d.getMonth();
    this.currYear = d.getFullYear();
    this.currDay = d.getDate();

};

// Goes to next month
Cal.prototype.nextMonth = function () {
    if (this.currMonth == 11) {
        this.currMonth = 0;
        this.currYear = this.currYear + 1;
    }
    else {
        this.currMonth = this.currMonth + 1;
    }
    this.showcurr();
};

// Goes to previous month
Cal.prototype.previousMonth = function () {
    if (this.currMonth == 0) {
        this.currMonth = 11;
        this.currYear = this.currYear - 1;
    }
    else {
        this.currMonth = this.currMonth - 1;
    }
    this.showcurr();
};

// Show current month
Cal.prototype.showcurr = function () {
    this.showMonth(this.currYear, this.currMonth);
};

// Show month (year, month)
Cal.prototype.showMonth = function (y, m) {

    var d = new Date()
        // First day of the week in the selected month
        , firstDayOfMonth = new Date(y, m, 1).getDay()
        // Last day of the selected month
        , lastDateOfMonth = new Date(y, m + 1, 0).getDate()
        // Last day of the previous month
        , lastDayOfLastMonth = m == 0 ? new Date(y - 1, 11, 0).getDate() : new Date(y, m, 0).getDate();


    var html = '<table>';

    // Write selected month and year
    $('#month').text(this.Months[m] + '월');


    // Write the header of the days of the week
    html += '<tr class="days">';
    for (var i = 0; i < this.DaysOfWeek.length; i++) {
        html += '<td>' + this.DaysOfWeek[i] + '</td>';
    }
    html += '</tr>';

    // Write the days
    var i = 1;
    do {

        var dow = new Date(y, m, i).getDay();

        // If Sunday, start new row
        if (dow == 0) {
            html += '<tr>';
        }
        // If not Sunday but first day of the month
        // it will write the last days from the previous month
        else if (i == 1) {
            html += '<tr>';
            var k = lastDayOfLastMonth - firstDayOfMonth + 1;
            for (var j = 0; j < firstDayOfMonth; j++) {
                html += '<td class="not-current">' + k + '</td>';
                k++;
            }
        }

        // Write the current day in the loop
        var chk = new Date();
        var chkY = chk.getFullYear();
        var chkM = chk.getMonth();
        var cal_month = this.Months[m];
        var cal_day = i;
        if(Number(cal_month) < 10) cal_month = "0" + cal_month;
        if(Number(cal_day) < 10) cal_day = "0" + cal_day;
        var data_id = chkY + "-" + cal_month + "-" + cal_day;
        
        if (chkY == this.currYear && chkM == this.currMonth && i == this.currDay) {
            html += '<td class="today day" id="cal'+ i +'" data-id="'+ data_id +'">' + i + '</td>';
        } else {
            html += '<td class="normal day" id="cal'+ i +'" data-id="'+ data_id +'">' + i + '</td>';
        	//html += '<td class="normal day" id="cal'+ i +'" data-id="'+ currMonth + "" + currYear +'">' + i + '</td>';
        }
        // If Saturday, closes the row
        if (dow == 6) {
            html += '</tr>';
        }
        // If not Saturday, but last day of the selected month
        // it will write the next few days from the next month
        else if (i == lastDateOfMonth) {
            var k = 1;
            for (dow; dow < 6; dow++) {
                html += '<td class="not-current">' + k + '</td>';
                k++;
            }
        }

        i++;
    } while (i <= lastDateOfMonth);

    // Closes table
    html += '</table>';

    // Write HTML to the div
    document.getElementById(this.divId).innerHTML = html;
    var cw = $("td").width();
    $('td').css({ 'height': cw + 'px' });
    //$('#cal1').css({'background':'yellow'});
    var listDate = [];
    <c:forEach var="list1" items="${list }">
		//console.log(${list1.consulting_seq});
		getDateRange("${fn:split(list1.start_dt,' ')[0]}","${fn:split(list1.end_dt,' ')[0]}",listDate);
		//$('td[data-id="'++'"]')
	</c:forEach>
		console.log('listdate :: ',listDate);
		for(var i =0;i<listDate.length;i++){
			$('td[data-id="'+listDate[i]+'"]').css({"background" : "green"});
		}
};

// On Load of the window
window.onload = function () {

    // Start calendar
    var c = new Cal("divCal");
    c.showcurr();

    // Bind next and previous button clicks
    getId('btnNext').onclick = function () {
        c.nextMonth();
    };
    getId('btnPrev').onclick = function () {
        c.previousMonth();
    };
    //var list = ${list}
    
    
}

// Get element by id
function getId(id) {
    return document.getElementById(id);
}

// 날짜 사잇값
function getDateRange(startDate, endDate, listDate) {
	
    var dateMove = new Date(startDate);
    var strDate = startDate;


    if (startDate == endDate)
    {
        var strDate = dateMove.toISOString().slice(0,10);
        listDate.push(strDate);
    }
    else
    {
        while (strDate < endDate)
        {
            var strDate = dateMove.toISOString().slice(0, 10);
            listDate.push(strDate);
            dateMove.setDate(dateMove.getDate() + 1);
        }
    }
    return listDate;

};




</script>



<div class="mainWrap">
	<main class="boardContents">
		<div class="board-container calendar">
			<div class="menu-container">
				<div class="title-container calendar">
					<button id="btnPrev" class="mainbtn transparent">
						<img src="/resources/src/assets/ic-arrow-left-black.svg" alt="이전" />
					</button>
					<span class="title" id="month">12월</span>
					<button id="btnNext" class="mainbtn transparent">
						<img src="/resources/src/assets/ic-arrow-right-black.svg" alt=다음 />
					</button>
				</div>
				<div class="calendar-container">
					<div id="divCal"></div>

					<div class="addschedule">
						<a href="./create">일정추가</a>
					</div>
				</div>
				<!--일정 상세 팝업 -->
				<!-- <div class="modalWrap">
                        <div class="modal selectedday">
                            <div class="title-container">
                                <p class="title">2020-12-21 ~ 2020-12-25</p>
                            </div>

                            <table>
                                <tr>
                                    <th class="title tiny bold">접수번호</th>
                                    <td class="desc bold green">I20201231000001</td>
                                </tr>
                                <tr>
                                    <th class="title tiny bold">작업일</th>
                                    <td>2020-12-21 ~ 2020-12-25</td>
                                    <td class="title tiny bold">5일</td>
                                </tr>
                                <tr>
                                    <th class="title tiny bold"></th>
                                    <td>경기도 성남시 분당구 미금로 215,
                                        101동 101호</td>
                                </tr>
                                <tr>
                                    <th class="title tiny bold">시공비</th>
                                    <td>50,000원</td>
                                </tr>
                                <tr>
                                    <th class="title tiny bold">상세내용</th>
                                    <td>20m2, xxx 자재 등 견적에 입력된 내용, 길이 길 경우</td>
                                </tr>
                            </table>

                            <div class="button-container">
                                <button class="mainbtn green" id="confirm">확인</button>
                            </div>
                        </div>
                    </div> -->
			</div>

			<div class="content-container">
				<div class="title-container">
					<p class="title">작업 일정</p>
				</div>

				<ul class="work-list">
					<c:forEach var="list" items="${list }">

						<li class="consulting_list"
							data-id="${fn:split(list.start_dt,' ')[0] }/${fn:split(list.end_dt,' ')[0] }">
							<table>
								<tr>
									<th class="title tiny bold">접수번호</th>
									<td class="desc bold green">${list.consulting_num}</td>
									<td class="title tiny bold" rowspan="4"><fmt:formatNumber value="${list.down_payment }" pattern="#,###"/>
										원</td>
								</tr>
								<tr>
									<th class="title tiny bold">작업일</th>
									<td>${fn:split(list.start_dt,' ')[0] }~ ${fn:split(list.end_dt,' ')[0] }</td>
								</tr>
								<tr>
									<th class="title tiny bold"></th>
									<td><c:choose>
											<c:when test="${list.consulting_send_type eq 'ON' }">
                                    		${list.m_addr }&nbsp;${list.m_addr_info }
                                    	</c:when>
											<c:otherwise>
                                    		${list.addr }&nbsp;${list.addr_info }
                                    	</c:otherwise>
										</c:choose></td>
								</tr>
								<tr>
									<th class="title tiny bold">상세내용</th>
									<td>20m2, xxx 자재 등 견적에 입력된 내용, 길이 길 경우</td>
								</tr>
							</table>
							<div class="button-container">
								<button id="accept" class="mainbtn green">수락</button>
								<button id="deny" class="mainbtn greeninvert">거부</button>
							</div>
						</li>
					</c:forEach>
					<li class="selected">
						<table>
							<tr>
								<th class="title tiny bold">접수번호</th>
								<td class="desc bold green">I20201231000001</td>
								<td data-id="sds" class="title tiny bold" rowspan="4">50,000원</td>
							</tr>
							<tr>
								<th class="title tiny bold">작업일</th>
								<td>2020-12-21 ~ 2020-12-25</td>
							</tr>
							<tr>
								<th class="title tiny bold"></th>
								<td>경기도 성남시 분당구 미금로 215, 101동 101호</td>
							</tr>
							<tr>
								<th class="title tiny bold">상세내용</th>
								<td>20m2, xxx 자재 등 견적에 입력된 내용, 길이 길 경우</td>
							</tr>
						</table>
						<div class="button-container">
							<button id="accept" class="mainbtn green">수락</button>
							<button id="deny" class="mainbtn greeninvert">거부</button>
						</div>
					</li>
					<li>
						<table>
							<tr>
								<th class="title tiny bold">접수번호</th>
								<td class="desc bold green">I20201231000001</td>
								<td class="title tiny bold" rowspan="4">50,000원</td>
							</tr>
							<tr>
								<th class="title tiny bold">작업일</th>
								<td>2020-12-21 ~ 2020-12-25</td>
							</tr>
							<tr>
								<th class="title tiny bold"></th>
								<td>경기도 성남시 분당구 미금로 215, 101동 101호</td>
							</tr>
							<tr>
								<th class="title tiny bold">상세내용</th>
								<td>20m2, xxx 자재 등 견적에 입력된 내용, 길이 길 경우</td>
							</tr>
						</table>
						<div class="button-container">
							<button id="accept" class="mainbtn green">수락</button>
							<button id="deny" class="mainbtn greeninvert">거부</button>
						</div>
					</li>
				</ul>
				<div class="modalWrap">
					<div class="modal requestdeny">
						<div class="title-container">
							<p class="title">작업 거부</p>
						</div>

						<textarea class="textarea" placeholder="거부 사유를 입력해주세요."></textarea>

						<div class="button-container">
							<button class="mainbtn transparent" id="cancel">취소</button>
							<button class="mainbtn green" id="yes">확인</button>
						</div>
					</div>
				</div>
				<ul class="pagebtn-list">
					<li class="pagebtn"><a href="#"><img
							src="/resources/src/assets/ic-page-left.svg"></a></li>
					<li class="pagebtn is-active"><a href="#">1</a></li>
					<li class="pagebtn"><a href="#">2</a></li>
					<li class="pagebtn"><a href="#">3</a></li>
					<li class="pagebtn"><a href="#">4</a></li>
					<li class="pagebtn"><a href="#">5</a></li>
					<li class="pagebtn"><a href="#"><img
							src="/resources/src/assets/ic-page-right.svg"></a></li>
				</ul>
			</div>
		</div>
	</main>
</div>