<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %> 
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<script>


$(function () {
    $("#deny").click(function () {
        $(".content-container > .modalWrap").fadeIn("fast").css("display", "flex");
    });
    $("#yes").click(function () {
        $(".modalWrap").fadeOut("fast");
        alert('작업이 취소되었습니다');
    });

    $("#cancel").click(function () {
        $(".modalWrap").fadeOut("fast");
    });

});


var Cal = function (divId) {

    //Store div id
    this.divId = divId;

    // Days of week, starting on Sunday
    this.DaysOfWeek = [
        '일',
        '월',
        '화',
        '수',
        '목',
        '금',
        '토'
    ];

    // Months, stating on January
    this.Months = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12'];

    // Set the current month, year
    var d = new Date();

    this.currMonth = d.getMonth();
    this.currYear = d.getFullYear();
    this.currDay = d.getDate();

};

// Goes to next month
Cal.prototype.nextMonth = function () {
    if (this.currMonth == 11) {
        this.currMonth = 0;
        this.currYear = this.currYear + 1;
    }
    else {
        this.currMonth = this.currMonth + 1;
    }
    this.showcurr();
};

// Goes to previous month
Cal.prototype.previousMonth = function () {
    if (this.currMonth == 0) {
        this.currMonth = 11;
        this.currYear = this.currYear - 1;
    }
    else {
        this.currMonth = this.currMonth - 1;
    }
    this.showcurr();
};

// Show current month
Cal.prototype.showcurr = function () {
    this.showMonth(this.currYear, this.currMonth);
};

// Show month (year, month)
Cal.prototype.showMonth = function (y, m) {

    var d = new Date()
        // First day of the week in the selected month
        , firstDayOfMonth = new Date(y, m, 1).getDay()
        // Last day of the selected month
        , lastDateOfMonth = new Date(y, m + 1, 0).getDate()
        // Last day of the previous month
        , lastDayOfLastMonth = m == 0 ? new Date(y - 1, 11, 0).getDate() : new Date(y, m, 0).getDate();


    var html = '<table>';

    // Write selected month and year
    $('#month').text(this.Months[m] + '월');


    // Write the header of the days of the week
    html += '<tr class="days">';
    for (var i = 0; i < this.DaysOfWeek.length; i++) {
        html += '<td>' + this.DaysOfWeek[i] + '</td>';
    }
    html += '</tr>';

    // Write the days
    var i = 1;
    do {

        var dow = new Date(y, m, i).getDay();

        // If Sunday, start new row
        if (dow == 0) {
            html += '<tr>';
        }
        // If not Sunday but first day of the month
        // it will write the last days from the previous month
        else if (i == 1) {
            html += '<tr>';
            var k = lastDayOfLastMonth - firstDayOfMonth + 1;
            for (var j = 0; j < firstDayOfMonth; j++) {
                html += '<td class="not-current">' + k + '</td>';
                k++;
            }
        }

        // Write the current day in the loop
        var chk = new Date();
        var chkY = chk.getFullYear();
        var chkM = chk.getMonth();
        if (chkY == this.currYear && chkM == this.currMonth && i == this.currDay) {
            html += '<td class="today">' + i + '</td>';
        } else {
            html += '<td class="normal" style="background:yellow;">' + i + '</td>';
        }
        // If Saturday, closes the row
        if (dow == 6) {
            html += '</tr>';
        }
        // If not Saturday, but last day of the selected month
        // it will write the next few days from the next month
        else if (i == lastDateOfMonth) {
            var k = 1;
            for (dow; dow < 6; dow++) {
                html += '<td class="not-current">' + k + '</td>';
                k++;
            }
        }

        i++;
    } while (i <= lastDateOfMonth);

    // Closes table
    html += '</table>';

    // Write HTML to the div
    document.getElementById(this.divId).innerHTML = html;
    var cw = $("td").width();
    $('td').css({ 'height': cw + 'px' });
};

// On Load of the window
window.onload = function () {

    // Start calendar
    var c = new Cal("divCal");
    c.showcurr();

    // Bind next and previous button clicks
    getId('btnNext').onclick = function () {
        c.nextMonth();
    };
    getId('btnPrev').onclick = function () {
        c.previousMonth();
    };
}

// Get element by id
function getId(id) {
    return document.getElementById(id);
}





</script>



    <div class="mainWrap">
        <main class="boardContents">
            <div class="board-container calendar">
                <div class="menu-container">
                    <div class="title-container calendar">
                        <button id="btnPrev" class="mainbtn transparent"><img
                                src="/resources/src/assets/ic-arrow-left-black.svg" alt="이전" /></button>
                        <span class="title" id="month">12월</span>
                        <button id="btnNext" class="mainbtn transparent"><img
                                src="/resources/src/assets/ic-arrow-right-black.svg" alt=다음 /></button>
                    </div>
                    <div class="calendar-container">
                        <div id="divCal"></div>

                        <div class="addschedule"><a href="./calendar-appointment.html">일정추가</a></div>
                    </div>
                    <!--일정 상세 팝업 -->
                    <!-- <div class="modalWrap">
                        <div class="modal selectedday">
                            <div class="title-container">
                                <p class="title">2020-12-21 ~ 2020-12-25</p>
                            </div>

                            <table>
                                <tr>
                                    <th class="title tiny bold">접수번호</th>
                                    <td class="desc bold green">I20201231000001</td>
                                </tr>
                                <tr>
                                    <th class="title tiny bold">작업일</th>
                                    <td>2020-12-21 ~ 2020-12-25</td>
                                    <td class="title tiny bold">5일</td>
                                </tr>
                                <tr>
                                    <th class="title tiny bold"></th>
                                    <td>경기도 성남시 분당구 미금로 215,
                                        101동 101호</td>
                                </tr>
                                <tr>
                                    <th class="title tiny bold">시공비</th>
                                    <td>50,000원</td>
                                </tr>
                                <tr>
                                    <th class="title tiny bold">상세내용</th>
                                    <td>20m2, xxx 자재 등 견적에 입력된 내용, 길이 길 경우</td>
                                </tr>
                            </table>

                            <div class="button-container">
                                <button class="mainbtn green" id="confirm">확인</button>
                            </div>
                        </div>
                    </div> -->
                </div>

                <div class="content-container">
                    <div class="title-container">
                        <p class="title">작업 일정</p>
                    </div>

                    <ul class="work-list">
                        <li class="selected">
                            <table>
                                <tr>
                                    <th class="title tiny bold">접수번호</th>
                                    <td class="desc bold green">I20201231000001</td>
                                    <td class="title tiny bold" rowspan="4">50,000원</td>
                                </tr>
                                <tr>
                                    <th class="title tiny bold">작업일</th>
                                    <td>2020-12-21 ~ 2020-12-25</td>
                                </tr>
                                <tr>
                                    <th class="title tiny bold"></th>
                                    <td>경기도 성남시 분당구 미금로 215,
                                        101동 101호</td>
                                </tr>
                                <tr>
                                    <th class="title tiny bold">상세내용</th>
                                    <td>20m2, xxx 자재 등 견적에 입력된 내용, 길이 길 경우</td>
                                </tr>
                            </table>
                            <div class="button-container">
                                <button id="accept" class="mainbtn green">수락</button>
                                <button id="deny" class="mainbtn greeninvert">거부</button>
                            </div>
                        </li>
                        <li>
                            <table>
                                <tr>
                                    <th class="title tiny bold">접수번호</th>
                                    <td class="desc bold green">I20201231000001</td>
                                    <td class="title tiny bold" rowspan="4">50,000원</td>
                                </tr>
                                <tr>
                                    <th class="title tiny bold">작업일</th>
                                    <td>2020-12-21 ~ 2020-12-25</td>
                                </tr>
                                <tr>
                                    <th class="title tiny bold"></th>
                                    <td>경기도 성남시 분당구 미금로 215,
                                        101동 101호</td>
                                </tr>
                                <tr>
                                    <th class="title tiny bold">상세내용</th>
                                    <td>20m2, xxx 자재 등 견적에 입력된 내용, 길이 길 경우</td>
                                </tr>
                            </table>
                            <div class="button-container">
                                <button id="accept" class="mainbtn green">수락</button>
                                <button id="deny" class="mainbtn greeninvert">거부</button>
                            </div>
                        </li>
                    </ul>
                    <div class="modalWrap">
                        <div class="modal requestdeny">
                            <div class="title-container">
                                <p class="title">작업 거부</p>
                            </div>

                            <textarea class="textarea" placeholder="거부 사유를 입력해주세요."></textarea>

                            <div class="button-container">
                                <button class="mainbtn transparent" id="cancel">취소</button>
                                <button class="mainbtn green" id="yes">확인</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>