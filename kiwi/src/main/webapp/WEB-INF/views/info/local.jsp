<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %> 
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<script>





</script>    
  
    <div class="mainWrap">
        <main class="boardContents">
            <div class="board-container servicemap">
                <div class="menu-container">
                    <div class="title-container">
                        <p class="title">안내</p>
                    </div>
                    <ul>
                        <li><a href="/info/use">이용안내</a></li>
                        <li class="is-active"><a href="/info/local">서비스 지역</a></li>
                        <li><a href="#">가격 안내</a></li>
                        <li><a href="/info/faq">자주 묻는 질문</a></li>
                    </ul>
                </div>

                <div class="content-container">
                    <div class="title-container">
                        <p class="title"><!-- <button class="board-menu"><img src="/resources/src/assets/ic-mobile-menu.svg" alt="메뉴보기"/></button> -->서비스 지역</p>
                    </div>
                    <div class="first">
                        <p class="title big">수도권</p>
                    </div>

                    <div class="second">
                        <img src="/resources/src/contents/servicemap.svg" alt="서비스 가능 지역" />
                        <div>
                            <p class="title green">현재 서비스 지역</p>
                            <p class="title normal">성남시 전체, 용인시 전체(처인구 일부 제외),
                                광주시 전체, 수원시 전체</p>
                        </div>
                        <div>
                            <p class="title">예정 지역</p>
                            <p class="title normal" style="color: gray;">현재 수도권을 중심으로 서비스 지역을 넓혀가고 있습니다. 새로운 서비스 지역 오픈 시 공지 및 알림으로 알려 드리겠습니다.
                                감사합니다.
                            </p>
                        </div>
                    </div>

                </div>
            </div>
        </main>
    </div>