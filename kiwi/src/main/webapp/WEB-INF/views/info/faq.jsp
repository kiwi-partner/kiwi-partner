<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %> 
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<script>

$(function() {
   $(document).on("click", ".title-container", function(){
		$(this).addClass(".detail-container");
        $(this).next().toggleClass('open');

    });
    

    
    
    $(document).on("click", ".tab-item", function(){
    	var faq_type = $(this).attr("data-type");
    	
        let container = $('.pagebtn-list');
        container.pagination({
            dataSource: function(done) { 
            	var param = {
                        "faq_type" : faq_type,
                        "type_seq" : "F"
                }
                ajaxCallPost("/api/v1/faq", param, function(res){
                	done(res.data);
                },function(){})
            },
            className: 'paginationjs-theme-green',

            callback: function (data, pagination) {
                var dataHtml = ' <li class="fold-listitem">';

                $.each(data, function (index, item) {
                    dataHtml += 
                    	 '<div class="title-container"><span class="title small">'+ item.title +'</span>'
                    	+'<span class="fold"><img src="/resources/src/assets/ic-arrow-bottom.svg" alt="펼치기"/></span></div>'
                        +'<div class="detail-container"><p>'+item.content
                    	+ '</p></div>';
                });

                dataHtml += '</li>';

                $(".fold-list").html(dataHtml);
            }
        })
    });

});


$(function () {
    let container = $('.pagebtn-list');
    container.pagination({
        dataSource: function(done) { 
        	var faq_type = $(".tab-item").eq(0).attr("data-type");
        	console.log("dadfdf"+faq_type);
        	var param = {
        			"faq_type" : faq_type,
                    "type_seq" : "F"
            }
            ajaxCallPost("/api/v1/faq", param, function(res){
            	done(res.data);
            },function(){})
        },
        className: 'paginationjs-theme-green',

        callback: function (data, pagination) {
            var dataHtml = ' <li class="fold-listitem">';

            $.each(data, function (index, item) {
                dataHtml += 
                	 '<div class="title-container"><span class="title small">'+ item.title +'</span>'
                	+'<span class="fold"><img src="/resources/src/assets/ic-arrow-bottom.svg" alt="펼치기"/></span></div>'
                    +'<div class="detail-container"><p>'+item.answer
                	+ '</p></div>';
            });

            dataHtml += '</li>';

            $(".fold-list").html(dataHtml);
        }
    })
})



</script>    

       <div class="mainWrap">
        <main class="boardContents">
            <div class="board-container">
                <div class="menu-container">
                    <div class="title-container">
                        <p class="title">안내</p>
                    </div>
                    <ul>
                        <li><a href="/info/use">이용안내</a></li>
                        <li><a href="/info/local">서비스 지역</a></li>
                        <li class="is-active"><a href="/info/faq">자주 묻는 질문</a></li>
                    </ul>
                </div>

                <div class="content-container">
                    <div class="title-container">
                        <p class="title"><!-- <button class="board-menu"><img src="/resources/src/assets/ic-mobile-menu.svg" alt="메뉴보기"/></button> -->자주 묻는 질문</p>
                    </div>

                    <ul class="tab">
       					<c:forEach var="item" items="${faqList }">
                      	  <li class="tab-item active" data-type="${item.common_seq }">${item.name }</li>
                       </c:forEach>
                    </ul>

                    <ul class="fold-list" style="margin-bottom:30px;">
                    	<!-- faq 리스트  -->
                    </ul>
				        <ul class="pagebtn-list">
                    	<!-- faq 페이징   -->
                   	   </ul>
                </div>
            </div>
        </main>
    </div>