<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %> 
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<script>





</script>    
  
    <div class="mainWrap">
        <main class="boardContents">
            <div class="board-container">
                <div class="menu-container">
                    <div class="title-container">
                        <p class="title">안내</p>
                    </div>
                    <ul>
                        <li class="is-active"><a href="/info/use">이용안내</a></li>
                        <li><a href="/info/local">서비스 지역</a></li>
                        <li><a href="#">가격 안내</a></li>
                        <li><a href="/info/faq">자주 묻는 질문</a></li>
                    </ul>
                </div>
                <div class="guide-container">
                    <div class="content-container first">
                        <img src="/resources/src/ic-symbol-mono.svg" alt="로고" />
                        <p class="title big">키위홈 이용 안내</p>
                    </div>

                    <div class="content-container second">
                        <div class="title-container">
                            <span class="number">1</span>
                            <p class="title big">홈 화면에서 원하시는 서비스 메뉴를 선택해 주세요.</p>
                            <p class="desc big">인테리어, 키위홈 스토어, 이사 / 입주청소 중 선택해 주세요.</p>
                        </div>
                        <img alt="설명 이미지" class="descimg" src="/resources/src/contents/howto/1.png">
                    </div>

                    <div class="content-container third">
                        <div class="title-container">
                            <span class="number">2</span>
                            <p class="title big">세부 메뉴 및 설치 물품을 선택해 주세요.</p>
                            <p class="desc big">인테리어, 이사/입주청소의 경우 견적이 필요한 서비스 아이콘을 선택해 주세요.</p>
                        </div>
                        <img alt="설명 이미지" class="descimg" src="/resources/src/contents/howto/2.png">
                    </div>

                    <div class="content-container fourth">
                        <div class="title-container">
                            <span class="number">3</span>
                            <p class="title big">작업에 필요한 정보를 입력해 주세요.</p>
                            <p class="desc big">인테리어, 이사/입주청소 세부 작업에 대한<br>
                                정보를 입력하시고 접수해 주시면 완료됩니다.
                            </p>
                        </div>
                        <img alt="설명 이미지" class="descimg" src="/resources/src/contents/howto/3.png">
                    </div>

                    <div class="content-container fifth">
                        <div class="title-container">
                            <span class="number">?</span>
                            <p class="title big">쇼룸 안내 및 문의 접수</p>
                            <p class="desc big">키위홈 쇼룸을 방문해 주시면
                                실제 자재 확인이나 방문 접수가 가능합니다.<br>문의가 있으신 경우 메시지나 전화를 통해 문의해 주시기 바랍니다.
                            </p>
                        </div>
                        <img alt="설명 이미지" class="check" src="/resources/src/contents/howto/check-circle.svg">

                    </div>
                </div>
            </div>

        </main>
    </div>