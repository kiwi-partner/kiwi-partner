<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %> 
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<script>

/* function init(){
	var member_seq = "${sessionScope.MEMBER.member_seq}";
    var param = {
            "member_seq" : member_seq
    }
    ajaxCallPost("/api/v1/msg/list", param, function(res){
        $(".message-list").html($("#msg-data").render(res));
    },function(){})
	
}

init(); */
$(function() {
 	$(".msg_file_btn").change(function(){
		ajaxUpload($(".msg_file_btn")[0].files[0], "msg", function(data_url) {
			$("input[name='file_url']").val(data_url);
		})
	})
    $(document).on("click", ".msg-btn", function(){
    	var member_seq = "${sessionScope.MEMBER.member_seq}";
		var content = $("input[name=content]");
		var file_url = $("input[name=file_url]");
        var param = {
                "member_seq" : member_seq,
                "content" : content.val(),
                "content_url" : file_url.val()
        }
        ajaxCallPost("/api/v1/msg/create", param, function(res){
            //$(".message-list").html($("#msg-data").render(res));
            location.reload(true);
        },function(){})
    });

});

</script>    
    
    	<script id="msg-data" type="text/x-jsrender">
			{{for data}}
                    <li class="message-sent">
                        <div >
                            <p class="message-content">
								{{:content}}
                            </p>
                            <p class="timestamp">{{:create_dt_yyyymmdd}}</p>
                        </div>
                    </li>
			{{/for}}
	</script>
    
    
    <div class="mainWrap">
        <main class="messageContents">
            <div class="message-container">
                <ul class="message-list">
                    <li class="message-receive">
                        <img class="avatar" src="/resources/src/ic-symbol.svg" />
                        <div>
                            <p class="message-content">안녕하세요. 고객님!
                                무엇을 도와드릴까요?</p>

                            <p class="timestamp"></p>
                        </div>
                    </li>
                    
					<c:forEach var="item" items="${msgList }">
					<!-- 상담사 답변  -->
					<c:if test="${item.re_msg_seq ne null}">
					<li class="message-receive">
                        <img class="avatar" src="/resources/src/ic-symbol.svg" />
                        <div>
                            <p class="message-content">
                            ${item.content}</p>

                            <p class="timestamp">${item.create_dt_yyyymmdd}</p>
                        </div>
                    </li>
                    </c:if>
                    <!-- 사용자 문의  -->
					<c:if test="${item.re_msg_seq eq null}">
					  <li class="message-sent">
                        <div >
                            <p class="message-content">
								${item.content}
                            </p>
                            <p class="timestamp">
								${item.create_dt_yyyymmdd}</p>
                        </div>
                    </li>
                    </c:if>
					</c:forEach>
                </ul>
                <div class="input-container">
                	<input type="hidden" name="file_url">
                    <input type="file" style="display:none;" class="msg_file_btn">
                    <button class="mainbtn" id="msg_input"><img src="/resources/src/assets/ic-plus.svg" alt="미디어 추가"></button>
                    <input class="input" name="content" type="text" placeholder="내용을 입력하세요."/>
                    <button class="mainbtn green msg-btn"><img src="/resources/src/assets/ic-send.svg" alt="전송"></button>
                </div>
            </div>
        </main>
    </div>