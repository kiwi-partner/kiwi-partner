<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>kiwihome -partner</title>
    <link rel="icon" href="/favicon.ico" />

    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="/resources/js/background.js"></script>
    <script src="/resources/js/header.js"></script>
    <!-- 
    <script src="/resources/js/pagination.js"></script>
    <script src="/resources/js/pagination.min.js"></script>
 	-->

    <script src="/resources/js/commonAjax.js"></script>
    <script src="/resources/js/custom.js"></script>
    
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Noto+Sans+KR:wght@100;300;400;500;700;900&display=swap"
        rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/resources/css/style.css" />
    <link rel="stylesheet" type="text/css" href="/resources/css/pagination.css" />
	
	
	<script src="https://www.jsviews.com/download/jsrender.js"></script>
	<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
	
	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jsrender/1.0.11/jsrender.min.js"></script>
	
	 <!-- 다음 카카오 맵  -->
	<script src="//t1.daumcdn.net/mapjsapi/bundle/postcode/prod/postcode.v2.js"></script>
	 <!-- 카카오 맵 api -->   
	 <script type="text/javascript" src="//dapi.kakao.com/v2/maps/sdk.js?appkey=51586c6d943cfc64cc5672295714511c&libraries=services,clusterer,drawing"></script>
	<!-- 페이징  api 
	 <script src="//cdnjs.cloudflare.com/ajax/libs/list.js/2.3.1/list.min.js"></script>
	 <script src="//cdnjs.cloudflare.com/ajax/libs/list.pagination.js/0.1.1/list.pagination.min.js"></script>  
	  -->  
	 <!-- 
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script> 
	 
	
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/paginationjs/2.1.4/pagination.min.js"></script>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/paginationjs/2.1.4/pagination.css"/>
	
</head>

<body>

	<tiles:insertAttribute name="headerAdmin" />
	<tiles:insertAttribute name="contentAdmin" />
	<tiles:insertAttribute name="footerAdmin" />


</body>
</html>