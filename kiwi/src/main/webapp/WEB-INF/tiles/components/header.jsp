<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %> 
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<script>

$(function(){
	 $(document).on("click", ".logout-btn", function(){
       location.href="/logout";
  	 });
	 /* 알림 확인하고 페이지 이동 */
	 $(document).on("click", ".noti_info", function(){
	       var seq = $(this).attr("data-seq");
	       var type = $(this).attr("data-type");
	       ajaxCallGet("/api/v1/alarm/check?seq="+seq, function(res){
				if(res.success){
					if(type == 'M'){
					  location.href="/mykiwi";
					}
					if(type == 'K'){
					 location.href="/";
					}
					
				}
			})
	   });
})
</script>
    <header class="mainHeader">
        <div class="topHeader">
            <div>
                <c:if test="${sessionScope.MEMBER eq null}"><a href="/" class="link">로그인</a></c:if>
                <c:if test="${sessionScope.MEMBER ne null}"><a href="/logout" class="link">로그아웃</a></c:if>
                <a href="/sign" class="link">회원가입</a>
                <a href="/cs/list" class="link">고객센터</a>
            </div>
        </div>
        <nav class="mainMenu">
            <span>
                <span class="logo-container">
                    <a href="/" class="logo"><img src="/resources/src/logo.png" alt="키위홈" /></a>
                </span>

                <span class="menu-container">
                    <ul class="mainMenulist">
                        <li class="mainMenulistitem" id="mykiwi"><a href="/mykiwi">마이키위</a></li>
                        <li class="mainMenulistitem" id="message"><a href="/message/list">메시지</a></li>
                        <li class="mainMenulistitem" id="noticeevent"><a href="/notice/list">공지사항∙이벤트</a>
                            <ul class="subMenulist">
                                <li class="subMenulistitem" id="notice" onclick="location.href='/notice/list'">
                                    공지사항
                                </li>
                                <li class="subMenulistitem" id="event" onclick="location.href='/notice/event'">
                                    이벤트
                                </li>
                            </ul>
                        </li>
                        <li class="mainMenulistitem" id="info"><a href="/info/use">안내</a>
                            <ul class="subMenulist">
                                <li class="subMenulistitem" id="guidance" onclick="location.href='/info/use'">
                                    이용안내
                                </li>
                                <li class="subMenulistitem" id="available" onclick="location.href='/info/local'">
                                    서비스 지역
                                </li>
                                <!-- 
                                <li class="subMenulistitem" id="bill"onclick="location.href='#'">
                                    가격 안내
                                </li>
                                 -->
                                <li class="subMenulistitem" id="faq" onclick="location.href='/info/faq'">
                                    자주 묻는 질문
                                </li>
                            </ul>
                        </li>
                        <li class="mainMenulistitem" id="showroom"><a href="/showroom/list">쇼룸 오시는 길</a></li>
                    </ul>
                </span>
            </span>
            <span>
                <span class="username">${sessionScope.MEMBER.name }</span>
                <a class="noti" href="#">
                    <div class="badge">${alramCount }</div>
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="18.286" viewBox="0 0 16 18.286">
                        <path id="Icon_awesome-bell" data-name="Icon awesome-bell"
                            d="M8,18.286A2.285,2.285,0,0,0,10.285,16H5.715A2.285,2.285,0,0,0,8,18.286Zm7.692-5.347c-.69-.741-1.981-1.857-1.981-5.51A5.64,5.64,0,0,0,9.142,1.887V1.143a1.142,1.142,0,1,0-2.284,0v.744A5.64,5.64,0,0,0,2.289,7.429C2.289,11.082,1,12.2.307,12.939A1.116,1.116,0,0,0,0,13.714a1.144,1.144,0,0,0,1.146,1.143H14.854A1.144,1.144,0,0,0,16,13.714,1.115,1.115,0,0,0,15.692,12.939Z"
                            fill="#606060" />
                    </svg>
                </a>

                <div class="modal notification">
                    <p class="title">알림</p>

                    <ul class="notificationList">
					<%@ include file="/WEB-INF/views/alram/list.jsp" %>
				<!-- 
					<c:forEach var="item" items="${alramList }">
                        <li class="notificationListitem">
                            <div class="container">
                                <span class="content">
                                    <span class="work-id">${item.title }</span>
                                    <span class="work-info">
                                        <div class="title tiny">${item.content }</div>
                                        <div class="desc small">${item.create_dt_yyyymmdd  }
                                        	<c:if test="${item.show_yn eq 'Y'}">NEW</c:if></div>
                                    </span>
                                </span>
                                <a href="#">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="6.792" height="11"
                                        viewBox="0 0 6.792 11">
                                        <path id="Icon_material-keyboard-arrow-right"
                                            data-name="Icon material-keyboard-arrow-right"
                                            d="M12.885,18.333l4.2-4.208-4.2-4.207,1.293-1.293,5.5,5.5-5.5,5.5Z"
                                            transform="translate(-12.885 -8.625)" fill="#606060" />
                                    </svg>

                                </a>
                            </div>
                        </li>
					</c:forEach>
					-->
                    </ul>
                </div>
            </span>
        </nav>
    </header>
    
     
    <header class="mobileHeader">
        <div class="mobileHeader-container">
            <span class="logo-container"><a href="/"><img src="/resources/src/ic-symbol.svg" /></a></span>

            <span class="action-container">
                <span class="username">${sessionScope.MEMBER.name }</span>
       
                <button class="menu" id="menu"><img src="/resources/src/assets/ic-menu.svg" alt="메뉴"></button>

                <div class="modal notification">
                    <p class="title">알림</p>

                    <ul class="notificationList">
						<%@ include file="/WEB-INF/views/alram/list.jsp" %>
					<!-- 
       				<c:forEach var="item" items="${alramList }">
                        <li class="notificationListitem">
                            <div class="container">
                                <span class="content">
                                    <span class="work-id">${item.title }</span>
                                    <span class="work-info">
                                        <div class="title tiny">${item.content }</div>
                                        <div class="desc small">${item.create_dt_yyyymmdd  }
                                        	<c:if test="${item.show_yn eq 'Y'}">NEW</c:if></div>
                                    </span>
                                </span>
                                <a href="#">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="6.792" height="11"
                                        viewBox="0 0 6.792 11">
                                        <path id="Icon_material-keyboard-arrow-right"
                                            data-name="Icon material-keyboard-arrow-right"
                                            d="M12.885,18.333l4.2-4.208-4.2-4.207,1.293-1.293,5.5,5.5-5.5,5.5Z"
                                            transform="translate(-12.885 -8.625)" fill="#606060" />
                                    </svg>

                                </a>
                            </div>
                        </li>
					</c:forEach>
						 -->
                    </ul>
                </div>
            </span>
        </div>
    </header>
    
    <nav class="mobileNav">
        <div class="mobileNav-container">

            <ul class="mainMenulist">
                <li class="mainMenulistitem" id="home"><a href="/"><img
                            src="/resources/src/assets/nav/nav-home.svg" alt="홈" /></a></li>
                <li class="mainMenulistitem" id="mykiwi"><a href="/mykiwi"><img
                            src="/resources/src/assets/nav/nav-mykiwi.svg" alt="마이키위" /></a></li>
                <li class="mainMenulistitem" id="message"><a href="#" class="noti">
                        <div class="badge">${alramCount }</div><img src="/resources/src/assets/nav/nav-noti.svg" alt="알림" />
                    </a></li>
                <li class="mainMenulistitem is-active" id="message"><a href="/message/list"><img
                            src="/resources/src/assets/nav/nav-msg.svg" alt="메시지" /></a></li>
            </ul>
        </div>
    </nav>

    <div class="modal menu">
        <div id="blank"></div>
        <div class="container">
            <div class="user-container">
                <div class="userinfo-container">
                    <p class="username">${sessionScope.MEMBER.name }</p>
                    <span>
                        <button class="mainbtn transparent" id="mysetting"><img src="/resources/src/assets/nav/ic-settings.svg"
                                alt="정보수정" onclick="location.href='/member/update'" /></button>
                        <button class="mainbtn transparent logout-btn" id="logout"><img src="/resources/src/assets/nav/ic-logout.svg"
                                alt="로그아웃" /></button>
                    </span>
                </div>

            </div>

            <div class="menu-container">
                <div class="topMenulist">
                    <a class="topMenulistitem" id="howto" href="/info/use">
                        <div><img src="/resources/src/assets/nav/ic-howto.svg" alt="이용안내" />
                            <p class="desc big">이용안내</p>
                        </div>
                    </a>
                    <a class="topMenulistitem" id="services" href="/info/local">
                        <div><img src="/resources/src/assets/nav/ic-services.svg" alt="서비스지역" />
                            <p class="desc big">서비스지역</p>
                        </div>
                    </a>
                    <!-- 
                    <a class="topMenulistitem" id="prices" href="#">
                        <div><img src="/resources/src/assets/nav/ic-price.svg" alt="가격안내" />
                            <p class="desc big">가격안내</p>
                        </div>
                    </a>
                     -->
                </div>
                <div class="mainMenulist">
                    <li class="mainMenulistitem" id="notice"><a href="/notice/list"><img
                                src="/resources/src/assets/nav/ic-notice.svg" alt="아이콘" />공지사항</a></li>
                    <li class="mainMenulistitem" id="event"><a href="/notice/event"><img
                                src="/resources/src/assets/nav/ic-event.svg" alt="아이콘" />이벤트</a></li>
                    <!-- 
                    <li class="mainMenulistitem" id="review"><a href="#"><img src="/resources/src/assets/nav/ic-review.svg"
                                alt="아이콘" />사용자평가</a></li> --> 
                    <li class="mainMenulistitem" id="faq"><a href="/info/faq"><img
                                src="/resources/src/assets/nav/ic-faq.svg" alt="아이콘" />자주 묻는 질문</a></li>
                    <li class="mainMenulistitem" id="showroom"><a href="/showroom/list"><img
                                src="/resources/src/assets/nav/ic-showroom.svg" alt="아이콘" />쇼룸 오시는 길</a></li>
                </div>
            </div>

            <footer class="mobileFooter">
                <div class="footer-container">
                    <div class="footer-left">
                        <img src="/resources/src/assets/logo-mono.svg" />
                        <img src="/resources/src/assets/contact-info.svg" />
                    </div>

                    <div class="footer-right">
                        <div class="row">
                            <a href="#" class="desc bold">개인정보처리방침</a>
                            <a href="#" class="desc">이용약관</a>
                            <a href="#" class="desc">고객센터</a>
                        </div>

                        <div class="row">
                            <p class="desc"><b>상호명:</b> 주식회사 디지털허그 | <b>주소:</b> 경기도 성남시 분당구 구미동 159 (CGV스퀘어) B동 2F
                                키위홈<br><b>대표:</b> 김희정 | <b>사업자등록:</b> 182-88-01065 | <b>이메일:</b> help@kiwihome.co.kr</p>
                        </div>

                        <div class="row">
                            <p class="desc">키위홈은 온라인마켓 플레이스만을 제공하며 리모델링 / 실내건축 / 인테리어 서비스의 분쟁과 책임, 계약사항은 입점회원사와 당사자간에
                                있습니다.</p>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
    </div>

    
    