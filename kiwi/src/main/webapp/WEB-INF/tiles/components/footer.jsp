<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %> 
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>


    <footer class="mainFooter">
        <div class="footer-container">
            <div class="footer-left">
                <img src="/resources/src/assets/logo-mono.svg" />
                <img src="/resources/src/assets/contact-info.svg" />
            </div>
            <div class="footer-right">
                <div class="row">
                    <a href="#" class="desc bold">개인정보처리방침</a>
                    <a href="#" class="desc">이용약관</a>
                    <a href="#" class="desc">고객센터</a>
                </div>

                <div class="row">
                    <p class="desc"><b>상호명:</b> 주식회사 디지털허그 | <b>주소:</b> 경기도 성남시 분당구 구미동 159 (CGV스퀘어) B동 2F
                        키위홈<br><b>대표:</b> 김희정 | <b>사업자등록:</b> 182-88-01065 | <b>이메일:</b> help@kiwihome.co.kr</p>
                </div>

                <div class="row">
                    <p class="desc">키위홈은 온라인마켓 플레이스만을 제공하며 리모델링 / 실내건축 / 인테리어 서비스의 분쟁과 책임, 계약사항은 입점회원사와 당사자간에 있습니다.</p>
                </div>
            </div>
        </div>
    </footer>
    <!-- 
        <footer class="mobileFooter">
                <div class="footer-container">
                    <div class="footer-left">
                        <img src="/resources/src/assets/logo-mono.svg" />
                        <img src="/resources/src/assets/contact-info.svg" />
                    </div>

                    <div class="footer-right">
                        <div class="row">
                            <a href="#" class="desc bold">개인정보처리방침</a>
                            <a href="#" class="desc">이용약관</a>
                            <a href="#" class="desc">고객센터</a>
                        </div>

                        <div class="row">
                            <p class="desc"><b>상호명:</b> 주식회사 디지털허그 | <b>주소:</b> 경기도 성남시 분당구 구미동 159 (CGV스퀘어) B동 2F
                                키위홈<br><b>대표:</b> 김희정 | <b>사업자등록:</b> 182-88-01065 | <b>이메일:</b> help@kiwihome.co.kr</p>
                        </div>

                        <div class="row">
                            <p class="desc">키위홈은 온라인마켓 플레이스만을 제공하며 리모델링 / 실내건축 / 인테리어 서비스의 분쟁과 책임, 계약사항은 입점회원사와 당사자간에
                                있습니다.</p>
                        </div>
                    </div>
                </div>
            </footer> 
             -->