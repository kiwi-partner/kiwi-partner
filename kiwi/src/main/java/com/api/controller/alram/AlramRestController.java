package com.api.controller.alram;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.api.controller.notice.NoticeRestController;
import com.api.service.MasterService;
import com.api.util.Common;
import com.api.util.ProHashMap;

@RestController 
@RequestMapping(value = "/api/v1/alarm/**")
public class AlramRestController {


	@Inject MasterService masterService;
	private static final Logger logger = LoggerFactory.getLogger(NoticeRestController.class);
	/**
	 * 알림 읽음 
	 */
	@GetMapping("/check")
	public ResponseEntity<Map<String, Object>> notiCheck(@RequestParam(value = "seq" , required = false) String seq, HttpSession session) throws Exception {
		ResponseEntity<Map<String, Object>> entity = null;		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("success", false);
		
		int record = masterService.dataUpdate("mapper.api.AlarmMapper", "noti", seq);
		if(record >0 ) {
			map.put("success", true);
		}
		try {entity = new ResponseEntity<Map<String, Object>>(map,HttpStatus.OK);} 
		catch (Exception e) {entity = new ResponseEntity<Map<String, Object>>(map,HttpStatus.BAD_REQUEST);}
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		
		return entity;
	}
}
