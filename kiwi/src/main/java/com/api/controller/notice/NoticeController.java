package com.api.controller.notice;

import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.api.service.MasterService;
import com.api.service.main.MainService;
import com.api.util.ProHashMap;



@Controller
@RequestMapping(value = "/notice/**")
public class NoticeController {
	@Inject MasterService masterService;


	@Inject MainService mainService; 

	private static final Logger logger = LoggerFactory.getLogger(NoticeController.class);
	/**
	 * 공지사항 페이지
	 */
	@GetMapping("/list") 
	public String dashboardNotice(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session) throws Exception {
		
		mainService.alarm(model, session);
//		ProHashMap proHashMap = new ProHashMap();
//		proHashMap.put("pg", 0);
//		proHashMap.put("page_block", 10);
//		proHashMap.put("type_seq", "SN");
//		
		List<ProHashMap> list =  (List<ProHashMap>) masterService.dataList("mapper.api.BoardMapper", "list", null);
		model.addAttribute("list", list);	
//		int record = masterService.dataCount("mapper.api.BoardMapper", "board_cnt", proHashMap);	
//		proHashMap.setTotalCount(record);
//		model.addAttribute("paging", proHashMap.getPageIngObj());
//			System.out.print("<<<<<<"+model);
		return "layout/notice/list";
	}
	
	/**
	 * 공지사항|이벤트  상세 페이지
	 */
	@GetMapping("/{seq}") 
	public String noticeDetail(Model model, @PathVariable String seq , HttpSession session) throws Exception {
		mainService.alarm(model, session);
		
		model.addAttribute("detail", masterService.dataRead("mapper.api.BoardMapper", "detail", seq));	
		
		return "layout/notice/read";
	}
	
	/**
	 * 이벤트 페이지
	 */
	@GetMapping("/event") 
	public String dashboardEvent(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session) throws Exception {
		ProHashMap proHashMap = new ProHashMap();
		proHashMap.put("pg", 0);
		proHashMap.put("page_block", 10);
		proHashMap.put("type_seq", "E");
		
		List<ProHashMap> list =  (List<ProHashMap>) masterService.dataList("mapper.api.BoardMapper", "list", proHashMap);
		model.addAttribute("list", list);	
		int record = masterService.dataCount("mapper.api.BoardMapper", "board_cnt", proHashMap);	
		proHashMap.setTotalCount(record);
		model.addAttribute("paging", proHashMap.getPageIngObj());
		mainService.alarm(model, session);
		
		return "layout/notice/event";
	}
	
	
}
