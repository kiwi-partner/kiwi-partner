package com.api.controller.category;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.api.service.MasterService;
import com.api.util.Common;
import com.api.util.ProHashMap;

@RestController 
public class CategoryRestController {
	
	@Inject MasterService masterService; 
	
	
	/**
	 * category_info 리스트
	 */
	@PostMapping("/api/v1/category/info")  
	public ResponseEntity<Map<String, Object>> category_info(HttpSession session, @RequestBody ProHashMap param) throws Exception {
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();

		HashMap paramMap = new HashMap(param);
		
		List<HashMap<String, Object>> retMap = null;
		retMap = (List) masterService.dataList("mapper.api.CategoryMapper", "info", paramMap);
		map.put("success", true);
		map.put("data", retMap);
		
		
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK); 
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity; 
	}
}
