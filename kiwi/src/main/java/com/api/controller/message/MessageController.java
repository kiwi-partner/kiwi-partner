package com.api.controller.message;

import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.api.controller.alram.AlramController;
import com.api.service.MasterService;
import com.api.service.main.MainService;



@Controller
@RequestMapping(value = "/message/**")
public class MessageController {
	@Inject MasterService masterService;

	@Inject MainService mainService; 
	

	private static final Logger logger = LoggerFactory.getLogger(MessageController.class);
	/**
	 * 메세지 전송페이지
	 */
	@GetMapping("/list") 
	public String dashboard(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session) throws Exception {
		mainService.alarm(model, session);
		mainService.msg(model, session);
	
		return "layout/message/list";
	}
	
	
	
}
