package com.api.controller.message;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.api.service.MasterService;
import com.api.service.main.MainService;
import com.api.util.Common;
import com.api.util.ProHashMap;

@RestController 
@RequestMapping(value = "/api/v1/msg/**")
public class MessageRestController {

	@Inject MainService mainService; 
	@Inject MasterService masterService;
	
	/**
	 *  message 리스트 
	 */
	@PostMapping("/list")  
	public ResponseEntity<Map<String, Object>> messageGet(HttpSession session, @RequestBody ProHashMap param) throws Exception {
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("success", false);

		HashMap paramMap = new HashMap(param);
		List<HashMap<String, Object>> retMap = null;
		
		retMap = (List<HashMap<String, Object>>) masterService.dataList("mapper.api.MessageMapper", "list", paramMap);
		if(retMap!=null) {
			map.put("success", true);
			map.put("data", retMap);
		}
		
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity; 
	}
	/**
	 * 메세지 보내기 
	 */
	@PostMapping("/create")  
	public ResponseEntity<Map<String, Object>> messageCreate(HttpSession session, @RequestBody ProHashMap param) throws Exception {
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("success", false);

		HashMap paramMap = new HashMap(param);
		HashMap<String, Object> retMap = null;
		
		int record  = masterService.dataCreate("mapper.api.MessageMapper", "create", paramMap);
		if(record >0 ) {
			
			map.put("success", true);
		}
		
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity; 
	}
	
}
