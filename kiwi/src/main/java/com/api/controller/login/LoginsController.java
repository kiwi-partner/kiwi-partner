package com.api.controller.login;

import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.api.service.MasterService;
import com.api.service.main.MainService;
import com.api.util.NiceSign;
import com.api.util.ProHashMap;



@Controller
public class LoginsController {
	
	@Inject MainService mainService; 
	@Inject MasterService masterService;

	
	/**
	 * 메인페이지
	 */
	@GetMapping("") 
	public String dashboard(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session) throws Exception {
		HashMap<String, Object> tempMemberEntity = (HashMap) session.getAttribute("MEMBER");
		//String login_type = (String) session.getAttribute("INFO");
		
		if(tempMemberEntity == null) {   
				
			return "notLayout/login/login";
		}else {	
			mainService.alarm(model, session);
			ProHashMap proHashMap = new ProHashMap();
			proHashMap.put("pg", 0);
			proHashMap.put("page_block", 10);
			proHashMap.put("type_seq", "SN");
			proHashMap.put("store_seq", "1");
			
			List<ProHashMap> list =  (List<ProHashMap>) masterService.dataList("mapper.api.MainMapper", "list", proHashMap);
			
			model.addAttribute("list", list);	
			int record = masterService.dataCount("mapper.api.MainMapper", "consulting_cnt", proHashMap);	
			proHashMap.setTotalCount(record);
			model.addAttribute("paging", proHashMap.getPageIngObj());
			return "layout/main/index";
		}
	}
	
	@GetMapping("/sign") 
	public String signPartner(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session) throws Exception {

		mainService.category(model);
		return "notLayout/login/sign";
	
	}

	@GetMapping("/nice") 
	public String signNice(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session) throws Exception {
		String check=NiceSign.commonNiceSign(session);
		model.addAttribute("data",check);
		return "notLayout/login/nice";
	
	}
	@GetMapping("/logout") 
	public String logoutSession(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session) throws Exception {
		session.invalidate();
		return "redirect:/";
	
	}
	@GetMapping("/return/fail") 
	public String returnFail(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session) throws Exception {
		return "notLayout/return/fail";
	
	}
	
	@GetMapping("/return/success") 
	public String returnSuccess(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session) throws Exception {
		HashMap<String, Object> resultMap = new HashMap();
		resultMap = NiceSign.resultNice(request, session);
		
		System.out.print("resultMap"+resultMap);
		model.addAttribute("data",resultMap);
		return "notLayout/return/success";
	}
	
}
