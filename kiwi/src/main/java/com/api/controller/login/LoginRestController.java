package com.api.controller.login;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.api.service.MasterService;
import com.api.service.main.MainService;
import com.api.util.Common;
import com.api.util.ProHashMap;

@RestController 
public class LoginRestController {

	
	@Inject MainService mainService; 
	@Inject MasterService masterService;
	 
	/**
	 * 로그인  
	*/
	@PostMapping("/api/v1/login")  
	public ResponseEntity<Map<String, Object>> login(Model model,HttpSession session, @RequestBody ProHashMap param) throws Exception {
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("success", false);

		HashMap paramMap = new HashMap(param);
		
		HashMap<String, Object> retMap = null;
		retMap = (HashMap) masterService.dataRead("mapper.LoginMapper", "login", paramMap);
		if (retMap != null) {
			retMap = (HashMap) masterService.dataRead("mapper.LoginMapper", "user_info_id", paramMap);
			session.setAttribute("MEMBER", retMap);
			
			map.put("success", true);
		}
		
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity; 
	} 
	
	
	/**
	 * 회원가입
	 */
	@PostMapping("/api/v1/sign")  
	public ResponseEntity<Map<String, Object>> join(HttpSession session, @RequestBody ProHashMap param) throws Exception {
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("success", false);

		HashMap paramMap = new HashMap(param);
		HashMap<String, Object> retMap = null;
		
		retMap = (HashMap) masterService.dataRead("mapper.LoginMapper", "user_info_id", paramMap);
		int record =0;
		if(retMap==null) {
			record += masterService.dataCreate("mapper.LoginMapper", "sign", paramMap);
			record += masterService.dataCreate("mapper.api.StoreMapper", "store_sign", paramMap);
			record += masterService.dataCreate("mapper.api.StoreMapper", "store_job", paramMap);
			if(record == 3 )
		//	session.setAttribute("MEMBER", retMap);
			map.put("success", true);
		}
		
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity; 
	}
	
	
	/**
	 * 사용자 정보
	 */
	@PostMapping("/api/v1/user/info/seq")  
	public ResponseEntity<Map<String, Object>> user_info(HttpSession session, @RequestBody ProHashMap param) throws Exception {
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();

		HashMap paramMap = new HashMap(param);
		
		HashMap<String, Object> retMap = null;
		retMap = (HashMap) masterService.dataRead("mapper.LoginMapper", "user_info_seq", paramMap); 
		map.put("data", retMap);
 
		
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity; 
	}
	
	/**
	 * 공통 중복체크 ( 메일, 아이디)
	 */
	@PostMapping("/api/v1/common/chk")  
	public ResponseEntity<Map<String, Object>> common_chk(HttpSession session, @RequestBody ProHashMap param) throws Exception {
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("success", false);

		HashMap paramMap = new HashMap(param);
		HashMap<String, Object> retMap = null;
		retMap = (HashMap) masterService.dataRead("mapper.LoginMapper", "common_chk", paramMap);
		
		if(retMap != null) {
			map.put("success", true);//중복되는게 잇을경우 true
		}
		
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity;  
	}

	/**
	 * 현재비밀번호 체크
	 */
	@PostMapping("/api/v1/chk/pw")  
	public ResponseEntity<Map<String, Object>> chk_pw(HttpSession session, @RequestBody ProHashMap param) throws Exception {
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("success", false);
		
		HashMap paramMap = new HashMap(param);
		HashMap<String, Object> retMap = null; 
		retMap = (HashMap) masterService.dataRead("mapper.LoginMapper", "common_chk_pw", paramMap);
		
		if(retMap != null) {
			map.put("success", true);
			map.put("data", retMap);
		}
		
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity;  
	}
	/**
	 * 회원탈퇴
	 */ 
	@PostMapping("/api/v1/member/delete")  
	public ResponseEntity<Map<String, Object>> member_delete(HttpSession session, @RequestBody ProHashMap param) throws Exception {
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("success", false);

		HashMap paramMap = new HashMap(param); 
		int record = masterService.dataUpdate("mapper.LoginMapper", "member_delete", paramMap);
		
		if(record > 0) {
			session.invalidate();
			map.put("success", true);
		}
		
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity;  
	}
}
