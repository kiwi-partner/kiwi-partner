package com.api.controller.info;

import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.api.controller.alram.AlramController;
import com.api.service.MasterService;
import com.api.service.main.MainService;
import com.api.util.ProHashMap;



@Controller
@RequestMapping(value = "/info/**")
public class InfoController {
	@Inject MasterService masterService;

	@Inject MainService mainService; 
	

	private static final Logger logger = LoggerFactory.getLogger(InfoController.class);
	/**
	 * 이용안내 페이지
	 */
	@GetMapping("/use") 
	public String usedashboard(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session) throws Exception {
		mainService.alarm(model, session);
			
		return "layout/info/use";
	}
	
	/**
	 *  서비스 이용지역 페이지
	 */
	@GetMapping("/local") 
	public String localdashboard(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session) throws Exception {
		mainService.alarm(model, session);
			
		return "layout/info/local";
	}
	
	/**
	 * faq 페이지
	 */
	@GetMapping("/faq") 
	public String faqdashboard( Model model, HttpSession session) throws Exception {
		mainService.alarm(model, session);
		mainService.boardType(model);
		return "layout/info/faq";
	}
	
}
