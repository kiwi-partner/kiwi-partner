package com.api.controller.info;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.api.service.MasterService;
import com.api.util.Common;
import com.api.util.ProHashMap;

@Controller
public class InfoRestController {

	private static final Logger logger = LoggerFactory.getLogger(InfoRestController.class);
	

	@Inject MasterService masterService;
	
	@PostMapping("/api/v1/faq")     
	public ResponseEntity<Map<String, Object>> content(HttpSession session, @RequestBody ProHashMap param) throws Exception { 
		ResponseEntity<Map<String, Object>> entity = null; 
		Map<String, Object> map = new HashMap<String, Object>();  
		
		HashMap paramMap = new HashMap(param);

		List<HashMap<String, Object>> retMap = (List) masterService.dataList("mapper.api.BoardMapper", "list", paramMap);
		
			map.put("success", true);
			map.put("data", retMap);
		
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity;  
	}
	
}
