package com.api.controller.member;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.api.service.MasterService;
import com.api.util.Common;
import com.api.util.NiceSign;
import com.api.util.ProHashMap;
import com.api.util.Util;



@RestController 
public class MemberRestController {
	@Inject MasterService masterService; 
	
	/**
	 * 회원 정보 변경 
	 */
	@PostMapping("/api/v1/member/update")  
	public ResponseEntity<Map<String, Object>> memberUpdate(HttpSession session, @RequestBody ProHashMap param) throws Exception {
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("success", false);
		HashMap paramMap = new HashMap(param);
		HashMap<String, Object> retMap = null;
		
		retMap = (HashMap) masterService.dataRead("mapper.LoginMapper", "user_info_seq", paramMap);
		int record =0;
		if(retMap!=null) {
			record += masterService.dataUpdate("mapper.LoginMapper", "update", paramMap);
			record += masterService.dataUpdate("mapper.api.StoreMapper", "update", paramMap);
			record += masterService.dataUpdate("mapper.api.StoreMapper", "update_job", paramMap);
			if(record == 3 )
		//	session.setAttribute("MEMBER", retMap);
			map.put("success", true);
		}
		
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity; 
	}
	
	
	/**
	 * 휴대폰으로 인증번호 보내기 
	 
	@PostMapping("/api/v1/member/auth/rand/number")  
	public ResponseEntity<Map<String, Object>> login(HttpSession session, @RequestBody ProHashMap param) throws Exception {
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("success", false);

		HashMap paramMap = new HashMap(param);
		
		
		
		HashMap<String, Object> retMap = new HashMap<String, Object>();
		retMap.put("authNumber", Util.makeAuthRandNumber());
		map.put("data", retMap);
		map.put("success", true);
		
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity; 
	}
	*/
	

	/**
	 * 휴대폰으로 nice인증 

	@GetMapping("/api/v1/member/nice/auth")  
	public ResponseEntity<Map<String, Object>> niceCheck(HttpSession session,HttpServletRequest request) throws Exception {
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("success", false);

		//HashMap paramMap = new HashMap(param);
		
		HashMap<String,Object> resultMap = NiceSign.resultNice(request, session);
		System.out.print("<>>>>>>>resultMap"+resultMap);
		
		map.put("data", resultMap);
		map.put("success", true);
		
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity; 
	}
 	*/
}
