package com.api.controller.member;

import java.util.HashMap;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.api.service.MasterService;
import com.api.service.main.MainService;

@Controller
@RequestMapping(value = "/member/**")
public class MemberController {

	
	@Inject MasterService masterService;
	@Inject MainService mainService; 
	
	
	/**
	 * 회원정보 수정페이지
	 */
	@GetMapping("/update") 
	public String dashboard(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session) throws Exception {
		HashMap<String, Object> tempMemberEntity = (HashMap) session.getAttribute("MEMBER");
		String member_seq = (String) tempMemberEntity.get("member_seq").toString();
		mainService.alarm(model, session);
		mainService.category(model);
		model.addAttribute("detail",masterService.dataRead("mapper.LoginMapper", "user_info_store", member_seq));	
		return "layout/member/update";
	}
}