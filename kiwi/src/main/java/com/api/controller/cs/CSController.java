package com.api.controller.cs;

import java.util.HashMap;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.api.service.MasterService;
import com.api.service.main.MainService;



@Controller
@RequestMapping(value = "/cs/**")
public class CSController {
	@Inject MasterService masterService;
	@Inject MainService mainService; 
	
	private static final Logger logger = LoggerFactory.getLogger(CSController.class);
	
	/**
	 * 고객센터 페이지
	 */
	@GetMapping("/list") 
	public String dashboard(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session) throws Exception {
		HashMap<String, Object> tempMemberEntity = (HashMap) session.getAttribute("MEMBER");
	
			return "layout/cs/list";
	}

	
}
