package com.api.controller.main;

import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.api.controller.alram.AlramController;
import com.api.service.MasterService;
import com.api.service.main.MainService;
import com.api.util.ProHashMap;



@Controller
public class MainController {
	@Inject MasterService masterService;
	@Inject MainService mainService; 
	


	private static final Logger logger = LoggerFactory.getLogger(MainController.class);
	/**
	 * mykiwi페이지
	 */
	@GetMapping("/mykiwi") 
	public String mykiwiDashboard(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session) throws Exception {
		mainService.alarm(model, session);
		
		ProHashMap proHashMap = new ProHashMap();
		proHashMap.put("pg", 0);
		proHashMap.put("page_block", 10);
		proHashMap.put("type_seq", "SN");
		proHashMap.put("store_seq", "1");
		
		List<ProHashMap> list =  (List<ProHashMap>) masterService.dataList("mapper.api.MainMapper", "mykiwi_list", proHashMap);
		
		model.addAttribute("list", list);	
		int record = masterService.dataCount("mapper.api.MainMapper", "mykiwi_cnt", proHashMap);	
		proHashMap.setTotalCount(record);
		model.addAttribute("paging", proHashMap.getPageIngObj());
		
		return "layout/main/mykiwi";
	}
	
	/**
	 * 일정 등록 페이지
	 */
	@GetMapping("/create") 
	public String createCalendar(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session) throws Exception {
		mainService.alarm(model, session);
		
		return "layout/main/create";
	}
	
}
