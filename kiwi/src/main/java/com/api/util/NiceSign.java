package com.api.util;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class NiceSign {

	
//    @Value("#{props['domain.url']}")
//	static
//    String domain_url;

	
	public static String commonNiceSign(HttpSession session) {
		String domain_url = "http://localhost:8080";
		NiceID.Check.CPClient niceCheck = new  NiceID.Check.CPClient();
		   
	    String sSiteCode = "BU224";			// NICE평가정보로부터 부여받은 사이트코드.
	    String sSitePassword = "QxBvgIBvo8WS";		// NICE평가정보로부터 부여받은 사이트 패스워드.
	    
	    String sRequestNumber = "REQ0000000001";        	// ��û ��ȣ, �̴� ����/�����Ŀ� ���� ������ �ǵ����ְ� �ǹǷ� 
	                                                    	// ��ü���� �����ϰ� �����Ͽ� ���ų�, �Ʒ��� ���� �����Ѵ�.
	    sRequestNumber = niceCheck.getRequestNO(sSiteCode);
	  	session.setAttribute("REQ_SEQ" , sRequestNumber);	// 세션에 저장.
	   	String sAuthType = "";      	// ������ �⺻ ����ȭ��, M: �ڵ���, C: �ſ�ī��, X: ����������
	   	
	   	String popgubun 	= "N";		//Y : ��ҹ�ư ���� / N : ��ҹ�ư ����
		String customize 	= "";		//������ �⺻ �������� / Mobile : �����������
		
		String sGender = ""; 			//������ �⺻ ���� ��, 0 : ����, 1 : ���� 
		
	    // CheckPlus(��������) ó�� ��, ��� ����Ÿ�� ���� �ޱ����� ���������� ���� http���� �Է��մϴ�.
		//����url�� ���� �� ������������ ȣ���ϱ� �� url�� �����ؾ� �մϴ�. ex) ���� �� url : http://www.~ ���� url : http://www.~
	    String sReturnUrl = domain_url+"/return/success";      // 성공시 URL
	    String sErrorUrl = domain_url+"/return/fail";          // 실패시 URL

	    // �Էµ� plain ����Ÿ�� �����.
	    String sPlainData = "7:REQ_SEQ" + sRequestNumber.getBytes().length + ":" + sRequestNumber +
	                        "8:SITECODE" + sSiteCode.getBytes().length + ":" + sSiteCode +
	                        "9:AUTH_TYPE" + sAuthType.getBytes().length + ":" + sAuthType +
	                        "7:RTN_URL" + sReturnUrl.getBytes().length + ":" + sReturnUrl +
	                        "7:ERR_URL" + sErrorUrl.getBytes().length + ":" + sErrorUrl +
	                        "11:POPUP_GUBUN" + popgubun.getBytes().length + ":" + popgubun +
	                        "9:CUSTOMIZE" + customize.getBytes().length + ":" + customize + 
							"6:GENDER" + sGender.getBytes().length + ":" + sGender;

	  //  System.out.print(">>>>>>>>sPlainData"+sPlainData);
	    String sMessage = "";
	    String sEncData = "";
	    
	    int iReturn = niceCheck.fnEncode(sSiteCode, sSitePassword, sPlainData);
	    if( iReturn == 0 )
	    {
	        sEncData = niceCheck.getCipherData();
	    }
	    else if( iReturn == -1)
	    {
	        sMessage = "암호화 시스템 오류";
	    }    
	    else if( iReturn == -2)
	    {
	        sMessage = "암호화 처리 오류";
	    }    
	    else if( iReturn == -3)
	    {
	        sMessage = "암호화 데이터 오류";
	    }    
	    else if( iReturn == -9)
	    {
	        sMessage = "입력정보 오류";
	    }    
	    else
	    {
	        sMessage = "�˼� ���� ���� �Դϴ�. iReturn : " + iReturn;
	    }
	   // System.out.print(">>>>>>>>"+sEncData);
		return sEncData;
	}
	
	public static HashMap<String,Object> resultNice(HttpServletRequest request,  HttpSession session){

	    NiceID.Check.CPClient niceCheck = new  NiceID.Check.CPClient();
	    String sEncodeData = requestReplace(request.getParameter("EncodeData").toString(), "encodeData");

	    String sSiteCode = "BU224";			// NICE평가정보로부터 부여받은 사이트코드.
	    String sSitePassword = "QxBvgIBvo8WS";		// NICE평가정보로부터 부여받은 사이트 패스워드.
	
	    String sCipherTime = "";			// ��ȣȭ�� �ð�
	    String sRequestNumber = "";			// ��û ��ȣ
	    String sResponseNumber = "";		// ���� ������ȣ
	    String sAuthType = "";				// ���� ����
	    String sName = "";					// ����
	    String sDupInfo = "";				// �ߺ����� Ȯ�ΰ� (DI_64 byte)
	    String sConnInfo = "";				// �������� Ȯ�ΰ� (CI_88 byte)
	    String sBirthDate = "";				// �������(YYYYMMDD)
	    String sGender = "";				// ����
	    String sNationalInfo = "";			// ��/�ܱ������� (���߰��̵� ����)
		String sMobileNo = "";				// �޴�����ȣ
		String sMobileCo = "";				// ��Ż�
	    String sMessage = "";
	    String sPlainData = "";
	    
	    int iReturn = niceCheck.fnDecode(sSiteCode, sSitePassword, sEncodeData);
		HashMap<String, Object> resultMap = new HashMap();
	    if( iReturn == 0 )
	    {
	        sPlainData = niceCheck.getPlainData();
	        sCipherTime = niceCheck.getCipherDateTime();
	        
	        // ����Ÿ�� �����մϴ�.
	        java.util.HashMap mapresult = niceCheck.fnParse(sPlainData);
	        
	        sRequestNumber  = (String)mapresult.get("REQ_SEQ");
	        sResponseNumber = (String)mapresult.get("RES_SEQ");
	        sAuthType		= (String)mapresult.get("AUTH_TYPE");
	        sName			= (String)mapresult.get("NAME");
			//sName			= (String)mapresult.get("UTF8_NAME"); //charset utf8 ���� �ּ� ���� �� ���
	        sBirthDate		= (String)mapresult.get("BIRTHDATE");
	        sGender			= (String)mapresult.get("GENDER");
	        sNationalInfo  	= (String)mapresult.get("NATIONALINFO");
	        sDupInfo		= (String)mapresult.get("DI");
	        sConnInfo		= (String)mapresult.get("CI");
	        sMobileNo		= (String)mapresult.get("MOBILE_NO");
	        sMobileCo		= (String)mapresult.get("MOBILE_CO");
	        
	        String session_sRequestNumber = (String)session.getAttribute("REQ_SEQ");
	        if(!sRequestNumber.equals(session_sRequestNumber))
	        {
	            sMessage = "세션값 불일치 .";
	            sResponseNumber = "";
	            sAuthType = "";
	        }
	        resultMap.put("name", sName);
	        resultMap.put("phone", sMobileNo);
	        return resultMap;
	    }
	    else if( iReturn == -1)
	    {
	        sMessage = "복호화 시스템 오류 .";
	    }    
	    else if( iReturn == -4)
	    {
	        sMessage = "복호화 처리 오류 .";
	    }    
	    else if( iReturn == -5)
	    {
	        sMessage = "복호화 해시 오류 .";
	    }    
	    else if( iReturn == -6)
	    {
	        sMessage = "복호화 데이터 오류 .";
	    }    
	    else if( iReturn == -9)
	    {
	        sMessage = "입력 정보 오류 .";
	    }    
	    else if( iReturn == -12)
	    {
	        sMessage = "CP비밀번호 불일치 .";
	    }    
	    else
	    {
	        sMessage = "세션값 불일치 . iReturn : " + iReturn;
	    }
		return null;
	}
	
	public static String requestReplace (String paramValue, String gubun) {
		  String result = "";
	        
	        if (paramValue != null) {
	        	
	        	paramValue = paramValue.replaceAll("<", "&lt;").replaceAll(">", "&gt;");

	        	paramValue = paramValue.replaceAll("\\*", "");
	        	paramValue = paramValue.replaceAll("\\?", "");
	        	paramValue = paramValue.replaceAll("\\[", "");
	        	paramValue = paramValue.replaceAll("\\{", "");
	        	paramValue = paramValue.replaceAll("\\(", "");
	        	paramValue = paramValue.replaceAll("\\)", "");
	        	paramValue = paramValue.replaceAll("\\^", "");
	        	paramValue = paramValue.replaceAll("\\$", "");
	        	paramValue = paramValue.replaceAll("'", "");
	        	paramValue = paramValue.replaceAll("@", "");
	        	paramValue = paramValue.replaceAll("%", "");
	        	paramValue = paramValue.replaceAll(";", "");
	        	paramValue = paramValue.replaceAll(":", "");
	        	paramValue = paramValue.replaceAll("-", "");
	        	paramValue = paramValue.replaceAll("#", "");
	        	paramValue = paramValue.replaceAll("--", "");
	        	paramValue = paramValue.replaceAll("-", "");
	        	paramValue = paramValue.replaceAll(",", "");
	        	
	        	if(gubun != "encodeData"){
	        		paramValue = paramValue.replaceAll("\\+", "");
	        		paramValue = paramValue.replaceAll("/", "");
	            paramValue = paramValue.replaceAll("=", "");
	        	}
	        	
	        	result = paramValue;
	            
	        }
	        return result;
	}
}
