package com.api.util;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class ApiCall {

	private static final Logger logger = LoggerFactory.getLogger(ApiCall.class);
    public static String getCall(String url) throws IOException{
        HashMap<String, Object> result = new HashMap<String, Object>();
        String jsonInString = "";

        HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory();
        RestTemplate restTemplate = new RestTemplate(factory);

        HttpHeaders header = new HttpHeaders();
        HttpEntity<?> entity = new HttpEntity<>(header);

        UriComponents uri = UriComponentsBuilder.fromHttpUrl(url).build();
        ResponseEntity<Map> resultMap = restTemplate.exchange(uri.toString(), HttpMethod.GET, entity, Map.class);
        result.put("statusCode", resultMap.getStatusCodeValue()); //http status code�� Ȯ��
        result.put("header", resultMap.getHeaders()); //��� ���� Ȯ��
        result.put("body", resultMap.getBody()); //���� ������ ���� Ȯ��

        //�����͸� ����� ���� �޾Ҵ��� Ȯ�� string���·� �Ľ�����
        ObjectMapper mapper = new ObjectMapper();
        try {
            jsonInString = mapper.writeValueAsString(resultMap.getBody());
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return jsonInString;
    }


    public static String postCall(String url, HashMap<String, Object> requestParam) throws IOException{
        HashMap<String, Object> result = new HashMap<String, Object>();
        String jsonInString = "";

        HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory();
        RestTemplate restTemplate = new RestTemplate(factory);


        HttpHeaders header = new HttpHeaders();
    	header.setContentType(MediaType.APPLICATION_JSON);

        UriComponents uri = UriComponentsBuilder.fromHttpUrl(url).build();

        HttpEntity<?> entity = new HttpEntity<>(getJsonStringFromMap(requestParam), header);
        
        
        ResponseEntity<Map> resultMap = restTemplate.exchange(uri.toString(), HttpMethod.POST, entity, Map.class);
    	
        
        result.put("statusCode", resultMap.getStatusCodeValue()); //http status code�� Ȯ��
        result.put("header", resultMap.getHeaders()); //��� ���� Ȯ��
        result.put("body", resultMap.getBody()); //���� ������ ���� Ȯ��

        //�����͸� ����� ���� �޾Ҵ��� Ȯ�� string���·� �Ľ�����
        ObjectMapper mapper = new ObjectMapper();
        try {
            jsonInString = mapper.writeValueAsString(resultMap.getBody());
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return jsonInString;
    }
    public static JsonObject jsonToObject(String str){
    	JsonParser jsonParser = new JsonParser();
        JsonObject object = (JsonObject) jsonParser.parse(str);
        return object;
    }
    public static JsonArray jsonToList(String str){
    	JsonParser jsonParser = new JsonParser();
	    JsonElement jsonElement = jsonParser.parse(str);
	    JsonArray list = jsonElement.getAsJsonArray();
    	return list;
    }
    /**
     * Map�� json���� ��ȯ�Ѵ�.
     *
     * @param map Map<String, Object>.
     * @return JSONObject.
     */
    public static JSONObject getJsonStringFromMap( Map<String, Object> map )
    {
        JSONObject jsonObject = new JSONObject();
        for( Map.Entry<String, Object> entry : map.entrySet() ) {
            String key = entry.getKey();
            Object value = entry.getValue();
            jsonObject.put(key, value);
        }
        
        return jsonObject;
    }
}
