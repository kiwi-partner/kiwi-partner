package com.api.service.main;

import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import com.api.persistence.MasterDao;

@Service
public class MainService {
	@Inject MasterDao MasterDao;

	
	public void main(Model model,HttpSession session) {
		alarm(model, session);
	    category(model);
	}
	
	public void category(Model model) {
		
	
		/**
		 * 회원가입시 카테고리 리스트 가져오기
		 */
		List<HashMap<String, Object>> categoryList = (List) MasterDao.dataList("mapper.api.CategoryMapper", "list", null);
		model.addAttribute("categoryList", categoryList);
		
	}
	
	public void alarm(Model model,HttpSession session) {
		
		HashMap<String, Object> sessionMap = null;
		sessionMap = (HashMap<String, Object>) session.getAttribute("MEMBER");
	
		HashMap<String, Object> rstMap = new HashMap<String, Object>();
		 
		if(sessionMap != null) { 
			rstMap.put("member_seq", sessionMap.get("member_seq"));
		}
		/**
		 * 알림 리스트 가져오기
		 */
		List<HashMap<String, Object>> alramList = (List) MasterDao.dataList("mapper.api.AlarmMapper", "list", rstMap);
		model.addAttribute("alramList", alramList);
		model.addAttribute("alramCount", MasterDao.dataCount("mapper.api.AlarmMapper", "alram_cnt", rstMap));
		
	}
	
	public void msg(Model model,HttpSession session) {
		
		HashMap<String, Object> sessionMap = null;
		sessionMap = (HashMap<String, Object>) session.getAttribute("MEMBER");
	
		HashMap<String, Object> rstMap = new HashMap<String, Object>();
		 
		if(sessionMap != null) { 
			rstMap.put("member_seq", sessionMap.get("member_seq"));
		}
		/**
		 * msg 리스트 가져오기
		 */
		List<HashMap<String, Object>> msgList = (List) MasterDao.dataList("mapper.api.MessageMapper", "list", rstMap);
		model.addAttribute("msgList", msgList);
		
	}
	
	public void boardType(Model model) {
		
		HashMap<String, Object> rstMap = new HashMap<String, Object>();
		 
		/**
		 * faq type 리스트 가져오기
		 */
		List<HashMap<String, Object>> faqList = (List) MasterDao.dataList("mapper.api.BoardMapper", "type", null);
		model.addAttribute("faqList", faqList);
		
	}
}
